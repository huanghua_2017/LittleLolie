
#include "WebsocketConn.h"
#include "../http/Request.h"
#include "../common/ErrorCode.h"
#include "../common/ComUtils.h"
#include "../loop/LoopService.h"
#include "WebsockerErrorcode.h"
#include "utils.h"
#include <event2/bufferevent.h>
#include <event2/bufferevent_ssl.h>
#include <event2/buffer.h>
#include <event2/http.h>
#include <event2/util.h>
#include <event2/event.h>
#include <assert.h>
#include <algorithm>

WebsocketConn::WebsocketConn() 
    : buffer_(nullptr)
    , request_(nullptr)
    , loop_(nullptr)
    , evconn_(nullptr)
{
    de_status_ = decode_status::PS_HEAD;
}

WebsocketConn::~WebsocketConn()
{
    assert(status_ == ConnState::Disconnected);
    if (request_) {
        evhttp_connection_free(evconn_);
        evhttp_request_free(request_);
    }
    if (type_ == ConnType::Outgoing && buffer_) {
        bufferevent_free(buffer_);
    }
}

std::error_code WebsocketConn::hook(LoopService* lp, const std::shared_ptr<Request> r)
{
    assert(lp && r);
    type_ = ConnType::Incoming;
    request_ = r->request();
    loop_ = lp;
    std::string tmpaddr = r->RemoteHost + ":" + std::to_string(r->RemotePort);
    int len = sizeof(remote_endpoint_);
    evutil_parse_sockaddr_port(tmpaddr.c_str(), (sockaddr*)&remote_endpoint_, &len);

    evconn_ = evhttp_request_get_connection(request_);
    buffer_ = evhttp_connection_get_bufferevent(evconn_);
    evhttp_request_own(request_);
    // drain all buffer.
    evbuffer_drain(evhttp_request_get_input_buffer(request_), -1);
    return hook_evb_callback();
}

std::error_code WebsocketConn::hook(LoopService* lp, bufferevent* bev)
{
    assert(lp && bev);
    type_ = ConnType::Outgoing;
    loop_ = lp;
    buffer_ = bev;

    socklen_t slen = sizeof(remote_endpoint_);
    getpeername(bufferevent_getfd(bev), (sockaddr*)&remote_endpoint_, &slen);

    auto ec = hook_evb_callback();
    
    if (!ec && open_fun_) {
        loop_->post([s = shared_from_this()](){
            s->open_fun_(s);
        });
    }
    return ec;
}

std::error_code WebsocketConn::hook_evb_callback()
{
    
    // hook bufferevent.
    init_local_by_fd(bufferevent_getfd(buffer_));

    bufferevent_setcb(buffer_, ws_read_handle, NULL, ws_event_handle, (void*)this);
    if (bufferevent_enable(buffer_, EV_READ | EV_WRITE) < 0) {
        return make_socket_error_code(EVUTIL_SOCKET_ERROR());
    }
    status_ = ConnState::Connected;
    return std::error_code();
}

void WebsocketConn::close(CloseStatus code)
{
    write(OPCODE::close, CloseStatusString[code]);
    shutdown();
}

void WebsocketConn::shutdown()
{
    if (status_ == ConnState::Disconnecting || status_ == ConnState::Disconnected) {
        return;
    }
    status_ = ConnState::Disconnecting;

    // 下次事件循环的时候关闭连接
    loop_->post([slf = shared_from_this()](){
        if (slf->request_) {
            evhttp_connection_free(slf->evconn_);
            evhttp_request_free(slf->request_);
        }
        if (slf->type_ == ConnType::Outgoing && slf->buffer_) {
            bufferevent_free(slf->buffer_);
        }
        slf->evconn_ = nullptr;
        slf->request_ = nullptr;
        slf->buffer_ = nullptr;
        slf->status_ = ConnState::Disconnected;
    });
}

void WebsocketConn::write(OPCODE code, const void* data, size_t len)
{
    assert(status_ == ConnState::Connected);
    if (loop_->is_in_loop_thread()) {
        std::string frame;
        bool outgoing = type_ == ConnType::Outgoing ? true : false;
        WebsocketFrame::build_frame(code, outgoing, data, len, frame);
        bufferevent_write(buffer_, frame.data(), frame.size());
        return;
    }
    loop_->post([code, slf = shared_from_this(), msg = std::string((const char*)data, len)]() {
        if (slf->status_.load() != ConnState::Connected) {
            return;
        }
        std::string frame;
        bool outgoing = slf->type_ == ConnType::Outgoing ? true : false;
        WebsocketFrame::build_frame(code, outgoing, msg.data(), msg.size(), frame);
        bufferevent_write(slf->buffer_, frame.data(), frame.size());
    });
}

int WebsocketConn::parse_message(const uint8_t* data, size_t len, size_t& drain)
{
    switch (de_status_)
    {
    case decode_status::PS_HEAD:
    {
        size_t minlen = std::min<size_t>(len, 2 - (size_t)de_offset_);
        memcpy(de_head_, data, minlen);
        de_offset_ += minlen;
        drain += minlen;
        data += minlen;
        len -= minlen;
        if (de_offset_ == 2) {
            de_offset_ = 0;
            de_status_ = decode_status::PS_PAYLOAD_LENGTH;
            fin = (de_head_[0] >> 7);
            rsv1 = (de_head_[0] & 0x40) >> 6;
            rsv2 = (de_head_[0] & 0x20) >> 5;
            rsv3 = (de_head_[0] & 0x10) >> 4;
            if (fin == 1) {
                opcode = (OPCODE)(de_head_[0] & 0x0F);
            }
            mask = (de_head_[1] >> 7);
            length = de_head_[1] & 0x7F;
            assert(!rsv1 && !rsv2 && !rsv3);
        }
        else { // len == 0 下次在解码帧
            break;
        }
    }
    case decode_status::PS_PAYLOAD_LENGTH:
    {
        size_t minlen = 0;
        if (length < 126) {
            de_status_ = decode_status::PS_MASK_KEY;
            de_length_ = length;
        }
        else if(length == 126) {
            minlen = std::min<size_t>(len, 2 - (size_t)de_offset_);
            memcpy( ((char*)&de_length_) + de_offset_, data, minlen);
            de_offset_ += minlen;
            drain += minlen;
            data += minlen;
            len -= minlen;
            if (de_offset_ == 2) {
                de_offset_ = 0;
                de_status_ = decode_status::PS_MASK_KEY;
                de_length_ = htons((uint16_t)de_length_);
            }
            else { // len == 0 下次在解码帧
                break;
            }
        }
        else {  // 127
            minlen = std::min<size_t>(len, 8 - (size_t)de_offset_);
            memcpy(((char*)&de_length_) + de_offset_, data, minlen);
            de_offset_ += minlen;
            drain += minlen;
            data += minlen;
            len -= minlen;
            if (de_offset_ == 8) {
                de_offset_ = 0;
                de_status_ = decode_status::PS_MASK_KEY;
                de_length_ = utils_htonll(de_length_);
            }
            else { // len == 0 下次在解码帧
                break;
            }
        }
    }
    case decode_status::PS_MASK_KEY:
    {
        size_t minlen = 0;
        if (mask == 1) {
            minlen = std::min<size_t>(len, 4 - (size_t)de_offset_);
            memcpy(de_mask_ + de_offset_, data, minlen);
            de_offset_ += minlen;
            drain += minlen;
            data += minlen;
            len -= minlen;
            if (de_offset_ == 4){
                de_offset_ = 0;
                de_status_ = decode_status::PS_PAYLOAD;
            }
            else {
                break;
            }
        }
        else {
            assert(type_ == ConnType::Outgoing);
            de_status_ = decode_status::PS_PAYLOAD;
        }
    }
    case decode_status::PS_PAYLOAD:
    {
        uint64_t minlen = std::min<uint64_t>(len, de_length_ - de_offset_);
        // 拷贝数据到 payload
        if (mask) {
            for (int i = 0; i < minlen; ++i)
            {
                de_payload_.push_back(data[i] ^ de_mask_[de_mask_offset_++ % 4]);
                if (de_mask_offset_ == 4) {
                    de_mask_offset_ = 0;
                }
            }
        }
        else {
            de_payload_.append((char*)data, (size_t)minlen);
        }
        de_offset_ += minlen;
        drain += (size_t)minlen;
        data += minlen;
        len -= (size_t)minlen;
       
        if (de_offset_ == de_length_) {
            // 已经解析了一个完整的数据包
            de_offset_ = 0;
            de_status_ = decode_status::PS_HEAD;
            // 如果当前帧的 fin == 1 才能说明这是一个真正的包
            if (fin) {
                return 1;
            }
        }
    }
    }

    return 0;
}

void WebsocketConn::process_complete_msg()
{
    // 解析出一个完整的数据，调用message_fun
    auto msg = std::make_shared<WebsocketFrame>();
    msg->opcode_ = opcode;
    msg->payload_.swap(de_payload_);
    // reset.
    de_status_ = decode_status::PS_HEAD;
    de_offset_ = 0;
    de_length_ = 0;
    de_mask_offset_ = 0;

    auto self = shared_from_this();
    if (opcode == OPCODE::binary || opcode == OPCODE::text) {
        if (message_fun_) {
            message_fun_(self, msg);
        }
    }
    else if (opcode == OPCODE::ping) {
        if (ping_fun_) {
            ping_fun_(self, msg);
        }
        else {
            // 回用pong消息
        }
    }
    else if (opcode == OPCODE::pong) {
        if (pong_fun_) {
            pong_fun_(self, msg);
        }
    }
    else if (opcode == OPCODE::close) {
        // 通知上层, 关闭套接字的任务交给上层自己去处理
        uint16_t ccode = -1;
        memcpy(&ccode, de_payload_.data(), std::min<size_t>(2, de_payload_.size()));
        if (close_fun_) {
            close_fun_(self, make_ws_close_error_code(ccode));
        }
    } 
}

void WebsocketConn::ws_read_handle(struct bufferevent *bev, void *ctx)
{
    auto self = (WebsocketConn*)ctx;
    auto input = bufferevent_get_input(bev);
    auto len = evbuffer_get_length(input);

    while (len > 0)
    {
        size_t drain = 0;
        auto clen = evbuffer_get_contiguous_space(input);
        auto cbuf = evbuffer_pullup(input, clen);
        // 解码websocket数据帧
        int ret = self->parse_message(cbuf, clen, drain);
        if (ret == 1) {
            self->process_complete_msg();
        }
        else if(ret == 0){
            // 数据不完整，nothing to do.
            ;
        }
        else {
            // 发生错误
            self->close((CloseStatus)ret);
            return;
        }
        len -= drain;
        evbuffer_drain(input, drain);
    }
}

void WebsocketConn::ws_event_handle(struct bufferevent *bev, short what, void *ctx)
{
    auto self = (WebsocketConn*)ctx;
    if (what & (BEV_EVENT_EOF | BEV_EVENT_ERROR))
    {
        fprintf(stderr, "event_handle error what=%d\n", what);
        std::error_code ec;
        if (what & BEV_EVENT_ERROR) {
            ec = make_ssl_error_code(bufferevent_get_openssl_error(bev));
        }
        else {
            ec = std::make_error_code(std::errc::connection_reset);
        }

        if (self->close_fun_) {
            self->close_fun_(self->shared_this(), ec);
        }
    }
}

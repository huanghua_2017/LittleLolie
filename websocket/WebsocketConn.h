
#pragma once

#include <memory>
#include <system_error>
#include <functional>
#include "WebsocketFrame.h"
#include "CloseStatus.h"
#include "../common/ConnStatus.h"
#include "../common/ConnAddrBase.h"


struct bufferevent;
struct evhttp_request;
struct evhttp_connection;

class LoopService;
class Request;

class WebsocketConn;
typedef std::shared_ptr<WebsocketConn> WebsocketConnPtr;

class WebsocketHandleBase
{
public:
    // websocket 连接建立成功回调
    typedef std::function<void(const WebsocketConnPtr&)>  open_fun;
    // websocket 连接断开回调
    typedef std::function<void(const WebsocketConnPtr&, const std::error_code&)> close_fun;
    // websocket 解码出一个消息帧回调
    typedef std::function<void(const WebsocketConnPtr&,const WebsocketFramePtr&)>  message_fun;
    // ping handle
    typedef message_fun    ping_fun;
    // pong handle
    typedef message_fun    pong_fun;
public:
    WebsocketHandleBase() {}
    virtual ~WebsocketHandleBase() {}

    void set_open_handle(const open_fun& f) {
        open_fun_ = f;
    }
    void set_close_handle(const close_fun& f) {
        close_fun_ = f;
    }
    void set_message_handle(const message_fun& f) {
        message_fun_ = f;
    }
    void set_ping_handle(const ping_fun& f) {
        ping_fun_ = f;
    }
    void set_pong_handle(const pong_fun& f) {
        pong_fun_ = f;
    }
protected:
    open_fun                open_fun_;
    close_fun               close_fun_;
    message_fun             message_fun_;
    ping_fun                ping_fun_;
    pong_fun                pong_fun_;
};

class WebsocketConn : public WebsocketHandleBase, 
    public ConnStatus,
    public ConnAddrBase,
    public std::enable_shared_from_this<WebsocketConn>
{
public:
    WebsocketConn();
    ~WebsocketConn();

    // 先发送关闭帧再关闭连接
    void close(CloseStatus code);
    
    // 直接关闭连接
    void shutdown();

    void write(OPCODE code, const std::string& msg) {
        write(code, msg.data(), msg.size());
    }

    void write(OPCODE code, const void* data, size_t len);
private:
    friend class Upgrader;
    friend class WebsocketDialer;

    // 服务端调用
    std::error_code hook(LoopService* lp, const std::shared_ptr<Request> r);

    // 客户端调用, 这儿会主动调用 open_fun回调
    std::error_code hook(LoopService* lp, bufferevent* bev);

private:
    WebsocketConnPtr shared_this() {
        return shared_from_this();
    }
    std::error_code hook_evb_callback();

    // 0  数据不完整
    // 1  解析出一个完整的数据包
    // other 发生错误
    // @prama drain 为消耗的数据长度
    int parse_message(const uint8_t* data, size_t len, size_t& drain);
    // 当解码出一个完整的帧后根据帧的类型进行消息处理
    void process_complete_msg();
    static void ws_read_handle(struct bufferevent *bev, void *ctx);
    static void ws_event_handle(struct bufferevent *bev, short what, void *ctx);
private:
    LoopService             *loop_;
    bufferevent             *buffer_;
    evhttp_request          *request_;
    evhttp_connection       *evconn_;

    //
    uint16_t  fin;
    uint16_t  rsv1;
    uint16_t  rsv2;
    uint16_t  rsv3;
    OPCODE  opcode;

    uint16_t  mask;
    uint16_t  length;

    // 解码websocket数据帧需要
    enum class decode_status {
        PS_HEAD, 
        PS_PAYLOAD_LENGTH, 
        PS_MASK_KEY, 
        PS_PAYLOAD
    } de_status_;
    uint64_t    de_offset_ = 0;
    uint8_t     de_head_[2];
    uint8_t     de_mask_[4];
    uint8_t     de_mask_offset_ = 0;
    uint64_t    de_length_ = 0;
    std::string de_payload_;
};
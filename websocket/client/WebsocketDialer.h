
#pragma once

#include "../../tlsconfig/TLSConfig.h"
#include "../../common/DialerBase.h"
#include "../../common/platform.h"
#include "../WebsocketConn.h"
#include <system_error>

struct bufferevent;

class EvURI;

class WebsocketDialer : public WebsocketHandleBase, public DialerBase
{
public:
    using DialerBase::DialerBase;

    void close();

    void set_dial_timeout(int ms) {
        timeout_ = ms;
    }

    // uri example: ws://localhost:1111/ws?a=1&b=2
    std::error_code async_dial(const std::string& uriStr, const TLSConfigPtr& tls);
    // 通过代理连接服务器
    std::error_code async_dial(const std::string& uriStr, const TLSConfigPtr& tls, const std::string& proxyStr);
private:
    static void read_handle(struct bufferevent *bev, void *ctx);
    static void event_handle(struct bufferevent *bev, short what, void *ctx);
    void make_sec_key();

    void conn_close_handle(const WebsocketConnPtr& conn, const std::error_code& e);

    std::error_code make_conn(ez_socket_t fd, sockaddr_storage* endpoint);

    std::error_code make_bufferevent(ez_socket_t fd, sockaddr_storage* addr);

    void build_handshake_request(const EvURI& uri);

    void verify_handshake();
private:
    // 这些变量都会交给conn进行托管，所以不需要在析构函数中进行释放
    bufferevent          *buffer_ = nullptr;
    std::string           rsp_handshake_msg_; // 服务器返回的握手信息
    std::string           rep_handshake_msg_; // 客户端发送的握手信息
    const static int      max_handshake_size_ = 4096;

    int                         timeout_ = 0;

    std::string             sec_key_;
    WebsocketConnPtr        conn_;
    TLSConfigPtr            tls_config_;
};

#include "WebsocketDialer.h"
#include "../../loop/LoopService.h"
#include "../../common/ErrorCode.h"
#include "../../common/ComUtils.h"
#include "../../common/EvURI.h"
#include "../WebsockerErrorcode.h"
#include "../../http/Request.h"
#include "../utils.h"
#include "../../proxy/ProxyBase.h"
#include <event2/bufferevent_ssl.h>
#include <event2/bufferevent.h>
#include <event2/buffer.h>
#include <event2/util.h>
#include <event2/http.h>
#include <event2/event.h>
#include <assert.h>
#include <random>
#include <sstream>

void WebsocketDialer::close()
{
    if (conn_) {
        conn_->shutdown();
        conn_.reset();
    }
}

std::error_code WebsocketDialer::make_bufferevent(ez_socket_t fd, sockaddr_storage* addr)
{
    if (tls_config_)
    {
        auto ssl = tls_config_->create_ssl();
        if (ssl == nullptr) {
            return make_custom_error_code(custom_error::tls_init_error);
        }
        buffer_ = bufferevent_openssl_socket_new(loop_->event_base(), fd, ssl,
            BUFFEREVENT_SSL_CONNECTING,
            BEV_OPT_CLOSE_ON_FREE | BEV_OPT_DEFER_CALLBACKS);
    }
    else {
        buffer_ = bufferevent_socket_new(loop_->event_base(), fd,
            BEV_OPT_CLOSE_ON_FREE | BEV_OPT_DEFER_CALLBACKS);
    }
    if (buffer_ == nullptr) {
        return make_custom_error_code(custom_error::bev_calloc_fail);
    }
    if (bufferevent_socket_connect(buffer_, (struct sockaddr*)addr, sizeof(*addr)) < 0) {
        return make_socket_error_code(EVUTIL_SOCKET_ERROR());
    }
    if (bufferevent_enable(buffer_, EV_READ | EV_WRITE) < 0) {
        return make_socket_error_code(EVUTIL_SOCKET_ERROR());
    }
    bufferevent_setcb(buffer_, read_handle, NULL, event_handle, (void*)this);
    return std::error_code();
}

void WebsocketDialer::build_handshake_request(const EvURI& uri)
{
    rep_handshake_msg_.clear();
    rsp_handshake_msg_.clear();

    // 添加websocket握手头部信息
    make_sec_key();
    
    std::string pq = uri.query().empty() ? uri.path() : (uri.path() + "?" + uri.query());
    std::stringstream ostr;
    ostr << "GET " << pq << " HTTP/1.1\r\n";
    ostr << "Host: " << uri.host() << ":" << uri.port() << "\r\n";
    ostr << "Upgrade: websocket\r\n";
    ostr << "Connection: Upgrade\r\n";
    ostr << "Sec-WebSocket-Key: " << sec_key_ << "\r\n";
    ostr << "Sec-WebSocket-Version: 13\r\n";
    ostr << "\r\n";

    rep_handshake_msg_ = ostr.str();
}

void WebsocketDialer::verify_handshake()
{
    auto errf = [this](const std::error_code& e) {
        bufferevent_free(buffer_);
        buffer_ = nullptr;
        if (close_fun_) {
            close_fun_(conn_, e);
        }
    };
    auto pos = rsp_handshake_msg_.rfind("\r\n\r\n");
    if (pos == std::string::npos && rsp_handshake_msg_.size() > max_handshake_size_) {
        errf(std::make_error_code(std::errc::message_size));
        return;
    }
    if (pos != rsp_handshake_msg_.size() - 4) {
        errf(std::make_error_code(std::errc::bad_message));
        return;
    }

    // TODO: 校验头部字段是否合法
    auto p = Request::create_incoming_request(rsp_handshake_msg_);
    if (p == nullptr) {
        errf(std::make_error_code(std::errc::bad_message));
        return;
    }
    if (p->Status != 101) {
        errf(make_custom_error_code(custom_error::ws_httpstatus_error));
        return;
    }
    // 校验头部字段是否正确
    if (evutil_ascii_strcasecmp(p->Header["Upgrade"].c_str(), "websocket") != 0) {
        errf(make_ws_error_code(ws_error::header_no_upgrade));
        return;
    }
    if (evutil_ascii_strcasecmp(p->Header["Connection"].c_str(), "upgrade") != 0) {
        errf(make_ws_error_code(ws_error::header_no_connect));
        return;
    }
    if (p->Header["Sec-Websocket-Accept"] != compute_accept_key(sec_key_)) {
        errf(make_ws_error_code(ws_error::header_sec_key_mismatch));
        return;
    }
    auto ec = conn_->hook(loop_, buffer_);
    if (ec) {
        errf(ec);
        return;
    }
    buffer_ = nullptr;
}

void WebsocketDialer::read_handle(struct bufferevent *bev, void *ctx)
{
    auto self = (WebsocketDialer*)ctx;
    auto input = bufferevent_get_input(bev);
    auto len = evbuffer_get_length(input);

    self->rsp_handshake_msg_.append((char*)evbuffer_pullup(input, len), len);
    evbuffer_drain(input, len);

    // 分析握手信息
    self->verify_handshake();
}

void WebsocketDialer::event_handle(struct bufferevent *bev, short what, void *ctx)
{
    auto self = (WebsocketDialer*)ctx;
    if (what & (BEV_EVENT_EOF | BEV_EVENT_ERROR))
    {
        std::error_code ec;
        if (what & BEV_EVENT_ERROR) {
            ec = make_ssl_error_code(bufferevent_get_openssl_error(bev));
        }
        else {
            ec = std::make_error_code(std::errc::connection_reset);
        }
        
        if (self->close_fun_) {
            self->close_fun_(self->conn_, ec);
        }
    }

    if (what & BEV_EVENT_CONNECTED)
    {
        // 连接成功了，开始发送握手信息
        bufferevent_write(self->buffer_, self->rep_handshake_msg_.data(), self->rep_handshake_msg_.size());
    }
}

std::error_code WebsocketDialer::async_dial(const std::string& uriStr, const TLSConfigPtr& tls)
{
    tls_config_ = tls;
    EvURI uri;
    std::error_code ec = uri.parse_from_string(uriStr);
    if (ec) {
        return ec;
    }
    sockaddr_storage endpoint = {};
    if (ec = uri_dns_resolve(uri, endpoint), ec) {
        return ec;
    }

    build_handshake_request(uri);
    ez_socket_t fd = socket(endpoint.ss_family, SOCK_STREAM, 0);
    if (fd == -1) {
        return make_socket_error_code(EVUTIL_SOCKET_ERROR());
    }
    if (evutil_make_socket_nonblocking(fd) < 0) {
        evutil_closesocket(fd);
        return make_socket_error_code(EVUTIL_SOCKET_ERROR());
    }
    return make_conn(fd, &endpoint); 
}

std::error_code WebsocketDialer::async_dial(const std::string& uriStr, const TLSConfigPtr& tls, const std::string& proxyStr)
{
    tls_config_ = tls;
    EvURI proxyUri;
    EvURI realUri;
    auto ec = proxyUri.parse_from_string(proxyStr);
    if (ec) {
        return ec;
    }
    ec = realUri.parse_from_string(uriStr);
    if (ec) {
        return ec;
    }

    // 根据代理的类型进行代理服务器连接操作
    ez_socket_t fd = -1;
    ec = ProxyBase::choice_auto(proxyUri, realUri, fd);
    if (ec) {
        return ec;
    }

    build_handshake_request(realUri);
    if (evutil_make_socket_nonblocking(fd) < 0) {
        evutil_closesocket(fd);
        return make_socket_error_code(EVUTIL_SOCKET_ERROR());
    }
    return make_conn(fd, nullptr);
}

void WebsocketDialer::make_sec_key()
{
    uint8_t key[16] = {};
    std::random_device rd;

    for (int i = 0; i < 16; ++i) {
        key[i] = rd() % 255;
    }
    sec_key_ = b64_encode(key, 16);
}

void WebsocketDialer::conn_close_handle(const WebsocketConnPtr& conn, const std::error_code& e)
{
    if (close_fun_) {
        close_fun_(conn, e);
    }
    conn_->shutdown();
    conn_.reset();
}

std::error_code WebsocketDialer::make_conn(ez_socket_t fd, sockaddr_storage* endpoint)
{
    using namespace std::placeholders;
    conn_ = std::make_shared<WebsocketConn>();
    conn_->set_open_handle(open_fun_);
    conn_->set_message_handle(message_fun_);
    conn_->set_ping_handle(ping_fun_);
    conn_->set_pong_handle(pong_fun_);
    conn_->set_close_handle(std::bind(&WebsocketDialer::conn_close_handle, this, _1, _2));

    return make_bufferevent(fd, endpoint);
}

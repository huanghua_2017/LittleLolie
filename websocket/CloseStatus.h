
#pragma once

#include <map>
#include <string>

enum class CloseStatus
{
    CloseNormalClosure              = 1000,
    CloseGoingAway                  = 1001,
    CloseProtocolError              = 1002,
    CloseUnsupportedData            = 1003,
    CloseNoStatusReceived           = 1005,
    CloseAbnormalClosure            = 1006,
    CloseInvalidFramePayloadData    = 1007,
    ClosePolicyViolation            = 1008,
    CloseMessageTooBig              = 1009,
    CloseMandatoryExtension         = 1010,
    CloseInternalServerErr          = 1011,
    CloseServiceRestart             = 1012,
    CloseTryAgainLater              = 1013,
};

static std::map<CloseStatus, std::string> CloseStatusString = {
    {CloseStatus::CloseNormalClosure              ,  " (normal)"                      },
    {CloseStatus::CloseGoingAway                  ,  " (going away)"                  },
    {CloseStatus::CloseProtocolError              ,  " (protocol error)"              },
    {CloseStatus::CloseUnsupportedData            ,  " (unsupported data)"            },
    {CloseStatus::CloseNoStatusReceived           ,  " (no status)"                   },
    {CloseStatus::CloseAbnormalClosure            ,  " (abnormal closure)"            },
    {CloseStatus::CloseInvalidFramePayloadData    ,  " (invalid payload data)"        },
    {CloseStatus::ClosePolicyViolation            ,  " (policy violation)"            },
    {CloseStatus::CloseMessageTooBig              ,  " (message too big)"             },
    {CloseStatus::CloseMandatoryExtension         ,  " (mandatory extension missing)" },
    {CloseStatus::CloseInternalServerErr          ,  " (internal server error)"       },
    {CloseStatus::CloseServiceRestart             ,  " (close service restart)"       },
    {CloseStatus::CloseTryAgainLater              ,  " (close try again later)"       },
};
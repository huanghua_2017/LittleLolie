
#pragma once

#include <memory>
#include <string>

class WebsocketFrame;
typedef std::shared_ptr<WebsocketFrame> WebsocketFramePtr;

enum class OPCODE {
    continuation = 0x0,
    text = 0x1,
    binary = 0x2,
    rsv3 = 0x3,
    rsv4 = 0x4,
    rsv5 = 0x5,
    rsv6 = 0x6,
    rsv7 = 0x7,
    close = 0x8,
    ping = 0x9,
    pong = 0xA,
    control_rsvb = 0xB,
    control_rsvc = 0xC,
    control_rsvd = 0xD,
    control_rsve = 0xE,
    control_rsvf = 0xF,

};

class WebsocketFrame : public std::enable_shared_from_this<WebsocketFrame>
{
public:
    OPCODE opcode() const {
        return opcode_;
    }
    std::string& payload() {
        return payload_;
    }
    const std::string& payload() const {
        return payload_;
    }
private:
    static void build_frame(OPCODE opcode, bool outgoing, const void* data, 
        size_t len, std::string& out);
private:
    friend class WebsocketConn;
    OPCODE      opcode_;
    std::string payload_;
};

#include <system_error>
#include <string>
#include "CloseStatus.h"

enum class ws_error
{
    method_not_get = 1,
    header_no_connect,
    header_no_upgrade,
    ws_version_error,
    header_no_sec_key,
    header_sec_key_mismatch,
};

class websocket_category : public std::error_category
{
public:
    char const * name() const noexcept {
        return "websocket_category error";
    }

    std::string message(int value) const {
        switch ((ws_error)value)
        {
        case ws_error::method_not_get:
            return "HTTP请求METHOD不是GET";
        case ws_error::header_no_connect:
            return "HTTP头部不含Connection字段";
        case ws_error::header_no_upgrade:
            return "HTTP头部不含Upgrade字段";
        case ws_error::ws_version_error:
            return "Sec-Websocket-Version字段值不匹配";
        case ws_error::header_no_sec_key:
            return "HTTP头部不含Sec-Websocket-Key字段";
        case ws_error::header_sec_key_mismatch:
            return "HTTP头部Sec-Websocket-Accept不匹配";
        default:
            break;
        }
        return "";
    }
};


inline const std::error_category& get_ws_category()
{
    static websocket_category instance;
    return instance;
}

inline std::error_code make_ws_error_code(ws_error e)
{
    return std::error_code(static_cast<int>(e), get_ws_category());
}

template<>
struct std::is_error_code_enum<ws_error> : std::true_type {};

/////////////////////////////////////////////////////////////////////////
// websocket 关闭帧的信息描述
class websocket_close_category : public std::error_category
{
public:
    char const * name() const noexcept {
        return "websocket_close_category error";
    }

    std::string message(int value) const {
        const std::string& str = CloseStatusString[(CloseStatus)value];
        return std::to_string(value) + str;
    }
};

inline const std::error_category& get_ws_close_category()
{
    static websocket_close_category instance;
    return instance;
}

inline std::error_code make_ws_close_error_code(uint16_t e)
{
    return std::error_code(static_cast<int>(e), get_ws_close_category());
}
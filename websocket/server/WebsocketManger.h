
#pragma once

#include <set>
#include <memory>
#include "../WebsocketConn.h"



class Request;
class ResponseWriter;

class WebsocketManger : public WebsocketHandleBase
{
public:

    // 如果协议升级成功，将调用open_fun
    std::error_code upgrade(std::shared_ptr<Request> r, std::shared_ptr<ResponseWriter> rw);
private:
    void close_handle(const WebsocketConnPtr& conn, const std::error_code& e);
private:
    std::set<WebsocketConnPtr>  con_set_;
};
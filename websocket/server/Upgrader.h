
#pragma once

#include "../WebsocketConn.h"
#include <assert.h>


class LoopService;
class Request;
class ResponseWriter;


class Upgrader
{
public:
    Upgrader(std::shared_ptr<Request> r, std::shared_ptr<ResponseWriter> w);

    WebsocketConnPtr upgrade(std::error_code& e);
    
private:
    LoopService                     *loop_;
    std::shared_ptr<Request>        req_;
    std::shared_ptr<ResponseWriter> rsp_;
};
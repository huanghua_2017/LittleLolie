
#pragma once

#include <stdint.h>
#include <string>

std::string b64_encode(const unsigned char* input, int length);

std::string compute_accept_key(const std::string& key);

uint64_t utils_htonll(uint64_t src);

#include "WebsocketFrame.h"
#include "utils.h"
#include <event2/util.h>
#include <random>

void WebsocketFrame::build_frame(OPCODE opcode, bool outgoing, const void* data, 
    size_t len, std::string& out)
{
    uint8_t hd[14] = { 0 };
    int head_len = 2;

    hd[0] |= 0x80;
    hd[0] |= (uint8_t)opcode;
    if (len <= 125) {
        hd[1] |= (uint8_t)len;
    }
    else if (len <= (uint16_t)0xFFFF) {
        head_len += 2;
        hd[1] |= 126;
        auto l = htons((uint16_t)len);
        memcpy(&hd[2], &l, 2);
    }
    else {
        head_len += 8;
        hd[1] |= 127;
        auto l = utils_htonll(len);
        memcpy(&hd[2], &l, 8);
    }

    if (outgoing)
    {
        //���봦��
        hd[1] |= 0x80;
        std::random_device rd;
        for (int i = 0; i < 4; ++i) {
            hd[head_len++] = rd() % 255;
        }
    }

    out.append((char*)hd, head_len);
   
    if (!outgoing) {
        out.append((const char*)data, len);
    }
    else {
        int mso = head_len - 4;
        for (size_t i = 0; i < len; ++i)
        {
            out.push_back(hd[mso++] ^ ((uint8_t*)data)[i]);  
            if (mso == head_len) {
                mso = head_len - 4;
            }     
        }
    }
}

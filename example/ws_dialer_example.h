#pragma once

#include "../websocket/client/WebsocketDialer.h"
#include <thread>

int ccount = 0;

inline void ws_dialer_example(const std::string& ip, const TLSConfigPtr& tls)
{
    WebsocketDialer dialer;

    dialer.set_open_handle([&](const WebsocketConnPtr& conn) {
        printf("wsclient connect success remote:%s local:%s\n",
            conn->remote_addr_string().c_str(), conn->local_addr_string().c_str());

        conn->write(OPCODE::text, "hello");
    });
    dialer.set_message_handle([](const WebsocketConnPtr& c, WebsocketFramePtr msg) {
        //printf("client recv: %s\n", msg->payload().c_str());
        ccount++;
        c->write(OPCODE::text, msg->payload());
    });
    dialer.set_close_handle([](const WebsocketConnPtr& c, const std::error_code& e) {
        printf("peer close %s\n", e.message().c_str());
    });

    dialer.async_dial(ip, tls);

    dialer.run();
}
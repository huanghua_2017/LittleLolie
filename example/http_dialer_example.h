#pragma once

#include "../http/httpclient/HTTPClient.h"


inline void http_dialer_example(const std::string& method, const std::string& uri, const std::string& body, const TLSConfigPtr& tls)
{
    HTTPClient dialer;

    RequestPtr r = Request::create_outgoing_request(method, uri, body);

    auto cf = [&](const RequestPtr& rsp, const std::error_code& e) {
        if (e) {
            printf("http error: %s\n", e.message().c_str());
            return;
        }
        printf("baidu :\n%s\n", rsp->Body.c_str());
       // dialer.Do(r);
    };

    auto cf1 = [&](const RequestPtr& rsp, const std::error_code& e) {
        if (e) {
            printf("http error: %s\n", e.message().c_str());
            return;
        }
        printf("vs :\n%s\n", rsp->Body.c_str());
        // dialer.Do(r);
    };

    dialer.Do(r, cf, tls);

    RequestPtr r1 = Request::create_outgoing_request(method, uri, body);
    dialer.Do(r1, cf1, tls);


    dialer.run();

}
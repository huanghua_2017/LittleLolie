#pragma once


#include "../udp/updclient/UdpClient.h"

int udpccount = 0;

inline void udp_dialer_examole(const std::string& ip)
{
    UdpClient dialer;

    dialer.set_message_handle([](const UdpTmpConnPtr& conn, const char* data, int len) {
        udpccount++;
        conn->write(data, len);
    });
    auto ec = dialer.dial(ip);
    if (ec) {
        printf("udp dial error: %s\n", ec.message().c_str());
    }

    dialer.conn()->write("udp hello!\n");

    dialer.run();

    getchar();
}
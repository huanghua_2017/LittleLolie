#pragma once

#include "../tcp/tcpserver/TCPServer.h"
#include <thread>
#include <assert.h>

int tcpscount = 0;

static void tcp_server_example(const std::string& ip, const TLSConfigPtr& tls)
{
    //server
    TCPServer svr(4);
    svr.set_connect_handle([](const TcpConnPtr& conn) {
        printf("server connect success remote:%s local:%s\n",
            conn->remote_addr_string().c_str(), conn->local_addr_string().c_str());

       // conn->write("welcome");
    });
    svr.set_disconnect_handle([](const TcpConnPtr& conn, const std::error_code& ec) {
        printf("disconnect: %s\n", ec.message().c_str());
    });

    svr.set_message_handle([](const TcpConnPtr& conn, const unsigned char* msg, int len) {
        //printf("%s\n", std::string((char*)msg, len).c_str());
        tcpscount++;
        conn->write(msg, len);
    });

//     assert(svr.set_use_ssl(true));
//     assert(!svr.set_certificate_file("server.pem", TLSConfig::SSLFileType::PEM));
//     assert(!svr.set_privateKey_file("server.pem", TLSConfig::SSLFileType::PEM));

    auto ec = svr.listen(ip, tls);
    if (ec) {
        printf("listen %s\n", ec.message().c_str());
    }
    svr.run();

    getchar();
    svr.stop();
}

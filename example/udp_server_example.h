#pragma once

#include "../udp/udpserver/UdpServer.h"

int udpscount = 0;

inline void udp_server_example(const std::string& ip)
{
    UdpServer svr;

    svr.set_message_handle([](const UdpTmpConnPtr& conn, const char* data, int len) {
        udpscount++;
        conn->write(data, len);
    });
    auto ec = svr.listen(ip);
    if (ec) {
        printf("udp listen error: %s\n", ec.message().c_str());
    }

    svr.run();

    getchar();
}
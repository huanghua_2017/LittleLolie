// example.cpp: 定义控制台应用程序的入口点。
//



#include "http_server_example.h"
#include "tcp_server_example.h"
#include "tcp_dialer_example.h"
#include "ws_server_example.h"
#include "ws_dialer_example.h"
#include "udp_server_example.h"
#include "udp_dialer_examole.h"
#include "http_dialer_example.h"
#include "../loop/DeadLineTimer.h"
#include "../loop/LoopService.h"


#ifdef _DEBUG
#pragma comment(lib, "../bin/LittleLolie_d.lib")
#pragma comment(lib, "../bin/libevent-2.1.8_d.lib")
#pragma comment(lib, "libeay32.lib")
#pragma comment(lib, "ssleay32.lib")
#pragma comment(lib, "ws2_32.lib")
#else
#pragma comment(lib, "../bin/LittleLolie.lib")
#pragma comment(lib, "../bin/libevent-2.1.8.lib")
#pragma comment(lib, "libeay32.lib")
#pragma comment(lib, "ssleay32.lib")
#pragma comment(lib, "ws2_32.lib")
#endif
int main()
{
    TLSConfigPtr tls = TLSConfig::make_ssl_config();
    //tls->set_certificate_file("server.pem", TLSConfig::SSLFileType::PEM);
    //tls->set_privateKey_file("server.pem", TLSConfig::SSLFileType::PEM);

    LoopService lp;
    DeadLineTimerPtr tmr = DeadLineTimer::create(&lp, 1000, true);
    std::thread([&]() {
        ws_server_example("0.0.0.0:4444", tls);
    }).detach();

    std::thread([&]() {
        ws_dialer_example("ws://127.0.0.1:4444/ws", tls);
    }).detach();
    
    tmr->start([](const std::error_code& e) {
        printf("websocket server count:%d, client count:%d\n", scount, ccount);
        scount = ccount = 0;
        printf("tcp server count:%d, client count:%d\n", tcpscount, tcpccount);
        tcpscount = tcpccount = 0;
        printf("udp server count:%d, client count:%d\n", udpscount, udpccount);
        udpscount = udpccount = 0;
    });

    
    std::thread([&]() {
        std::this_thread::yield();
        tcp_dialer_example("tcp://127.0.0.1:5555", tls);
    }).detach();

    std::thread([&]() {
        tcp_server_example("0.0.0.0:5555", tls);
    }).detach();

    std::thread([&]() {
        http_server_example("0.0.0.0:6666", nullptr);
    }).detach();

    std::thread([]() {
        udp_server_example("0.0.0.0:7777");
    }).detach();

    std::thread([]() {
        udp_dialer_examole("udp://127.0.0.1:7777");
    }).detach();

    std::thread([&]() {
        http_dialer_example("GET", "https://www.baidu.com/", "", tls);
    }).detach();

    lp.run();

    return 0;
}


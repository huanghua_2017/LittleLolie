#pragma once

#include "../http/httpserver/HTTPServer.h"

static void http_server_example(const std::string& ip, const TLSConfigPtr& tls)
{
    HTTPServer svr;

    struct aop1 {
        bool filter_handler(RequestPtr r, ResponseWriterPtr rw) {
            printf("aop1...\n");
            return true;
        }
    };
    struct aop2 {
        bool filter_handler(RequestPtr r, ResponseWriterPtr rw) {
            printf("aop2...\n");
            return true;
        }
    };

    svr.register_handle("/", [](RequestPtr r, ResponseWriterPtr rw) {
    
    });

    svr.register_handle<HTTPMethod::REQ_GET, HTTPMethod::REQ_POST>("/test", [&](RequestPtr r, ResponseWriterPtr rw) {
        for (auto& h : r->Header){
            printf("%s:%s\n", h.first.c_str(), h.second.c_str());
        }
        for (auto& h : r->From){
            printf("%s:%s\n", h.first.c_str(), h.second.c_str());
        }
        printf("Method:%s\nProto:%s\n", r->Method.c_str(), r->Proto.c_str());
        printf("URI:%s\nRequestURI:%s\n", r->Uri.c_str(), r->Path.c_str());

        printf("body:%s\n", r->Body.c_str());

        rw->write_chunk_begin(HTTPStatus::StatusOK);
        rw->write_chunk("Reply\n");
        rw->write_chunk(r->Body);
        rw->write_chunk_end();
    }, aop1{}, aop2{});

    auto ec = svr.listen(ip, tls);
    if (ec) {
        printf("listen:%s\n", ec.message().c_str());
    }

    
    svr.run();

    getchar();
    svr.stop();
}
#pragma once

#include "../tcp/tcpdialer/TCPDialer.h"
#include "../loop/DeadLineTimer.h"
#include <thread>
#include <chrono>

int tcpccount = 0;

static void tcp_dialer_example(const std::string& ip, const TLSConfigPtr& tls)
{
    TCPDialer dialer;
    dialer.set_connect_handle([](const TcpConnPtr& conn) {
        printf("client connect success remote:%s local:%s\n", 
            conn->remote_addr_string().c_str(), conn->local_addr_string().c_str());

        conn->write("GET / HTTP/1.1\r\n\r\n");
    });
    dialer.set_disconnect_handle([](const TcpConnPtr& conn, const std::error_code& ec) {
        printf("tcp disconnect: %s\n", ec.message().c_str());
    });

    dialer.set_message_handle([](const TcpConnPtr& conn, const unsigned char* msg, int len) {
       // printf("%s\n", std::string((char*)msg, len).c_str());
        tcpccount++;
        conn->write(msg, len);
    });

    auto ec = dialer.dial(ip, tls);
    if (ec) {
        printf("dialer %s\n", ec.message().c_str());
    }
    //std::this_thread::sleep_for(std::chrono::seconds(1));

    //dialer.conn()->write("123333333333333");

    auto timer = DeadLineTimer::create(dialer.loop(), 5000, false);
    timer->start([&](auto ec) {
       // dialer.stop();
    });


    dialer.run();
}
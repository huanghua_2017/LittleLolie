
#pragma once


#include <system_error>
#include "../common/platform.h"

class EvURI;

class ProxyBase
{
public:
    ProxyBase() = default;
    virtual ~ProxyBase() = default;

    // 自动选择代理方式进行连接
    static std::error_code choice_auto(const EvURI& proxyUri, const EvURI& realUri, ez_socket_t& fd);

    // @param fd 到代理服务器的描述符，处于非阻塞状态！！！
    virtual std::error_code proxy_dial(const EvURI& proxyUri,
        const EvURI& realUri, ez_socket_t& fd) = 0;

    // 阻塞模式连接
    std::error_code blocking_connect(const sockaddr_storage& endpoint, ez_socket_t& fd);

    // 设置读取超时时间
    std::error_code set_read_timeout(ez_socket_t fd, int ms);
};



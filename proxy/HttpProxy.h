

#pragma once

#include "ProxyBase.h"

class HttpProxy : public ProxyBase
{
public:
    virtual std::error_code proxy_dial(const EvURI& proxyUri,
        const EvURI& realUri, ez_socket_t& fd) override;

};
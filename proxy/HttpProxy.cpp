

#include "HttpProxy.h"
#include "../common/EvURI.h"
#include "../common/ComUtils.h"
#include "../common/ErrorCode.h"
#include <event2/util.h>
#include <sstream>

std::error_code HttpProxy::proxy_dial(const EvURI& proxyUri,
    const EvURI& realUri, ez_socket_t& fd)
{
    sockaddr_storage remote = {};
    auto ec = uri_dns_resolve(proxyUri, remote);
    if (ec) {
        return ec;
    }
    ec = blocking_connect(remote, fd);
    if (ec) {
        return ec;
    }

    // 设置read超时
    ec = set_read_timeout(fd, 5000);
    if (ec) {
        evutil_closesocket(fd);
        return ec;
    }

    std::stringstream istr;
    istr << "CONNECT " << realUri.host() << ":" << realUri.port() << " HTTP/1.1\r\n";
    if (!realUri.userinfo().empty()) {
        istr << "Proxy-Authorization: Basic " << realUri.userinfo() << "\r\n";
    }
    istr << "\r\n";

    // send connect message.
    auto len = send(fd, istr.str().data(), istr.str().size(), 0);
    if (len != istr.str().size()) {
        evutil_closesocket(fd);
        return make_socket_error_code(EVUTIL_SOCKET_ERROR());
    }

    // recv connect response.  
    char buf[4096] = {};
    len = 0;
    do {
        auto ret = recv(fd, buf + len, sizeof(buf) - len, 0);
        if (ret <= 0 || len + ret >= sizeof(buf)) {
            evutil_closesocket(fd);
            return make_socket_error_code(EVUTIL_SOCKET_ERROR());
        }
        len += ret;
    } while (strncmp(buf + len - 4, "\r\n\r\n", 4) != 0 || len < 4);

    // 得到http响应码
    std::string _;
    int code = 0;
    std::stringstream ostr(buf);
    ostr >> _ >> code;
    if (code != 200) {
        evutil_closesocket(fd);
        return make_custom_error_code(custom_error::ws_httpstatus_error);
    }
    // 将代理服务器的连接套接字设置为非阻塞，
    if (evutil_make_socket_nonblocking(fd) < 0) {
        evutil_closesocket(fd);
        return make_socket_error_code(EVUTIL_SOCKET_ERROR());
    }
    return std::error_code();
}



#include "ProxyBase.h"
#include "../common/ErrorCode.h"
#include "../common/EvURI.h"
#include "HttpProxy.h"
#include <event2/util.h>


std::error_code ProxyBase::choice_auto(const EvURI& proxyUri, const EvURI& realUri, ez_socket_t& fd)
{
    // 根据代理的类型进行代理服务器连接操作
    if (proxyUri.schem() == "http") {
        HttpProxy p;
        auto ec = p.proxy_dial(proxyUri, realUri, fd);
        if (ec) {
            return ec;
        }
    }
    else {
        return std::make_error_code(std::errc::invalid_argument);
    }
    return std::error_code();
}

std::error_code ProxyBase::blocking_connect(const sockaddr_storage& endpoint, ez_socket_t& fd)
{
    fd = socket(endpoint.ss_family, SOCK_STREAM, 0);
    if (fd < 0) {
        return make_socket_error_code(EVUTIL_SOCKET_ERROR());
    }

    int rc = ::connect(fd, (sockaddr*)&endpoint, sizeof(endpoint));
    if (rc != 0) {
        evutil_closesocket(fd);
        return make_socket_error_code(EVUTIL_SOCKET_ERROR());
    }
    return std::error_code();
}

std::error_code ProxyBase::set_read_timeout(ez_socket_t fd, int ms)
{
    // solaris，不支持。
#ifdef _WIN32
    // windows 的参数和linux的参数不一样
    if (setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, (char*)&ms, sizeof(ms)) < 0) {
        return make_socket_error_code(EVUTIL_SOCKET_ERROR());
    }
#else
    timeval val;
    val.tv_sec = ms / 1000;
    val.tv_usec = ms % 1000 * 1000;

    if (setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, (char*)&val, sizeof(val)) < 0) {
        return make_socket_error_code(EVUTIL_SOCKET_ERROR());
    }
#endif
    return std::error_code();
}

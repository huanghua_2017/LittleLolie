INCLUDE = -Ilibevent-2.1.8/include -Ilibevent-2.1.8/WIN32-Code
objs := $(patsubst %.cpp,%.o,$(wildcard *.cpp))

main:$(objs)  
	ar -cr libLittleLolie.a $^  
	
 
%.o : %.cpp   
	g++ -c $(INCLUDE) -o $@ $< -std=c++11 
clean:  
	rm *.o  

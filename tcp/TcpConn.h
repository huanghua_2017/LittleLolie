
#pragma once

#include "../common/ConnAddrBase.h"
#include "../common/ConnStatus.h"
#include <memory>
#include <system_error>
#include <functional>

struct bufferevent;
struct ssl_st;



class LoopService;

class TcpConn;
typedef std::shared_ptr<TcpConn> TcpConnPtr;


class TcpConnHandleBase
{
public:
    typedef std::function<void(const TcpConnPtr&)>          connect_fun;
    // 当连接发生错误的时候调用，主要是通知应用程序当前连接已经无效，主动关闭的连接不会发出通知
    typedef std::function<void(const TcpConnPtr&, const std::error_code&)>   disconnect_fun;
    typedef std::function<void(const TcpConnPtr&, const unsigned char*, int)> message_fun;

public:
    TcpConnHandleBase(){}
    virtual ~TcpConnHandleBase() {}

    void set_connect_handle(const connect_fun& f) {
        connect_fun_ = f;
    }
    void set_disconnect_handle(const disconnect_fun& f) {
        disconnect_fun_ = f;
    }
    void set_message_handle(const message_fun& f) {
        message_fun_ = f;
    }
protected:
    //////////////////////////////////////////////////////////////////////////
    connect_fun             connect_fun_;
    disconnect_fun          disconnect_fun_;
    message_fun             message_fun_;
};


class TcpConn : public ConnStatus, 
    public TcpConnHandleBase,
    public ConnAddrBase,
    public std::enable_shared_from_this<TcpConn>
{
    // 当连接被主动关闭的时候调用， 主要是要让tcpserver知道当前连接被关闭了好关闭事件池
    typedef std::function<void(const TcpConnPtr&)>          close_fun;
public:
    // ssl is nullptr  普通连接 ssl not nullptr ssl连接
    TcpConn(LoopService* ls, struct ssl_st* ssl);

    ~TcpConn();

    LoopService* loop() {
        return loop_;
    }
    void set_close_fun(const close_fun& f) {
        close_fun_ = f;
    }

    // 将在下次事件循环中关闭连接，请一定注意！
    // 在关闭连接后将调用 上层注册的 close_fun_函数
    void close();

    ez_socket_t fd() const {
        return fd_;
    }

    void write(const void* data, size_t length);

    void write(const std::string& data) {
        write(data.data(), data.size());
    }
    bool set_timeout(int rms, int wms);

private:
    friend class TCPServer;
    friend class TCPDialer;

    std::error_code update(ez_socket_t fd, int ssl_state, const sockaddr_storage& addr);

    // TCPServer only
    std::error_code attach(ez_socket_t fd, const sockaddr_storage& addr);

    // TCPDialer only
    std::error_code dial(ez_socket_t fd, const sockaddr_storage& addr);

    // TCPDialer only
    std::error_code attach_proxy(ez_socket_t fd);
private:
    TcpConnPtr shared_this() {
        return shared_from_this();
    }
    std::error_code enable_event();
    static void read_handle(struct bufferevent *bev, void *ctx);
    static void event_handle(struct bufferevent *bev, short what, void *ctx);
private:
    LoopService             *loop_;
    bufferevent             *buffer_;
    ssl_st                  *ssl_;
    ez_socket_t             fd_;

    close_fun               close_fun_;
};

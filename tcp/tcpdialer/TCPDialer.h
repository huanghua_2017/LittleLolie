
#pragma once

#include "../../tlsconfig/TLSConfig.h"
#include "../TcpConn.h"
#include "../../common/DialerBase.h"


class TCPDialer : public TcpConnHandleBase, public DialerBase
{
public:
    using DialerBase::DialerBase;

    // uri example: tcp://localhost:1111
    // 同步连接成功后也将调用connect_fun_
    std::error_code dial(const std::string& uriStr, const TLSConfigPtr& tls) {
        return create_connect(uriStr, false, tls);
    }

    // 异步连接，返回成功，但是实际没有成功时会触发 disconnect_fun_
    std::error_code async_dial(const std::string& uriStr, const TLSConfigPtr& tls) {
        return create_connect(uriStr, true, tls);
    }

    // 使用代理模式连接服务器
    std::error_code dial(const std::string& uriStr, const TLSConfigPtr& tls, const std::string& proxyStr);

    void close();

    TcpConnPtr conn() {
        return conn_;
    }
private:
    // 
    std::error_code create_connect(const std::string& uriStr, bool async, const TLSConfigPtr& tls);
    // 
    TcpConnPtr make_conn(std::error_code& ec);
private:
    TcpConnPtr              conn_;
    TLSConfigPtr            tls_config_;
};
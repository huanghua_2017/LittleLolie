
#include "TCPDialer.h"

#include <event2/util.h>
#include <event2/http.h>
#include "../../loop/LoopService.h"
#include "../../common/ErrorCode.h"
#include "../../common/ComUtils.h"
#include "../../common/EvURI.h"
#include "../../proxy/ProxyBase.h"
#include <assert.h>




void TCPDialer::close()
{
    if (conn_ == nullptr) {
        return;
    }
    conn_->close();
    conn_.reset();
}

TcpConnPtr TCPDialer::make_conn(std::error_code& ec)
{
    TcpConnPtr conn;
    if (tls_config_)
    {
        auto ssl = tls_config_->create_ssl();
        if (ssl == nullptr) {
            ec = make_custom_error_code(custom_error::tls_init_error);
            return nullptr;
        }
        conn = std::make_shared<TcpConn>(loop_, ssl);
    }
    else {
        conn = std::make_shared<TcpConn>(loop_, nullptr);
    }

    conn->set_connect_handle(connect_fun_);
    // 不处理 close_fun_ 事件
    conn->set_disconnect_handle(disconnect_fun_);
    conn->set_message_handle(message_fun_);

    return conn;
}

std::error_code TCPDialer::create_connect(const std::string& uriStr, bool async, const TLSConfigPtr& tls)
{
    tls_config_ = tls;
    EvURI uri;
    auto ec = uri.parse_from_string(uriStr);
    if (ec) {
        return ec;
    }
    sockaddr_storage endpoint = {};
    ec = uri_dns_resolve(uri, endpoint);
    if (ec) {
        return ec;
    }
    
    ez_socket_t fd = socket(AF_INET, SOCK_STREAM, 0);
    if (fd == -1) {
        return make_socket_error_code(EVUTIL_SOCKET_ERROR());
    }
    if (async && evutil_make_socket_nonblocking(fd) < 0) {
        evutil_closesocket(fd);
        return make_socket_error_code(EVUTIL_SOCKET_ERROR());
    }

    TcpConnPtr conn = make_conn(ec);
    if (ec) {
        return ec;
    }

    if ((ec = conn->dial(fd, endpoint))) {
        evutil_closesocket(fd);
        return ec;
    }

    // 线程可能不一样
    loop_->dispatch([=]() {
        if (conn_) {
            conn_->close();
        }
        conn_ = conn;
    });

    // 连接成后将在tcpconn 的event中触发连接成功事件，在哪儿会回调 connect_fun_;
    return std::error_code();
}

std::error_code TCPDialer::dial(const std::string& uriStr, const TLSConfigPtr& tls, const std::string& proxyStr)
{
    tls_config_ = tls;
    EvURI proxyUri;
    EvURI realUri;
    auto ec = proxyUri.parse_from_string(proxyStr);
    if (ec) {
        return ec;
    }
    ec = realUri.parse_from_string(uriStr);
    if (ec) {
        return ec;
    }

    // 根据代理的类型进行代理服务器连接操作
    ez_socket_t fd = -1;
    ec = ProxyBase::choice_auto(proxyUri, realUri, fd);
    if (ec) {
        return ec;
    }

    TcpConnPtr conn = make_conn(ec);
    if (ec) {
        return ec;
    }
    
    ec = conn->attach_proxy(fd);
    if (ec) {
        evutil_closesocket(fd);
        return ec;
    }
    // 线程可能不一样
    loop_->dispatch([=]() {
        if (conn_) {
            conn_->close();
        }
        conn_ = conn;
        // 执行connect_fun回调
    });

    return std::error_code();
}
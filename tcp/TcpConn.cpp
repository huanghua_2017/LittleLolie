

#include "TcpConn.h"
#include "../loop/LoopService.h"
#include "../common/ErrorCode.h"
#include "../common/ComUtils.h"
#include <event2/bufferevent_ssl.h>
#include <event2/bufferevent.h>
#include <event2/buffer.h>
#include <event2/util.h>
#include <event2/event.h>
#include <assert.h>
#include <openssl/err.h>



TcpConn::TcpConn(LoopService* ls, struct ssl_st* ssl)
    : loop_(ls)
    , ssl_(ssl)
    , fd_(-1)
    , buffer_(nullptr)
{
}

TcpConn::~TcpConn()
{
    printf(__FUNCTION__"\n");
    assert(status_ == ConnState::Disconnected);
    if (buffer_) {
        bufferevent_free(buffer_);
    }
}

void TcpConn::write(const void* data, size_t length)
{
    assert(status_ == ConnState::Connected);

    auto err_fun = [this]() {
        status_ = ConnState::Disconnected;
        auto self = shared_from_this();
        if (disconnect_fun_) {
            disconnect_fun_(self, make_socket_error_code(EVUTIL_SOCKET_ERROR()));
        }
        if (close_fun_) {
            close_fun_(self);
        }
        bufferevent_free(buffer_);
        buffer_ = nullptr;
    };
    if (loop_->is_in_loop_thread()) {
        if (status_.load() != ConnState::Connected) {
            return;
        }
        if (bufferevent_write(buffer_, data, length) < 0) {
            err_fun();
        }
        return;
    }
    loop_->post([slf = shared_from_this(), err_fun, msg = std::string((char*)data, length)](){
        if (slf->status_.load() != ConnState::Connected) {
            return;
        }
        if (bufferevent_write(slf->buffer_, msg.data(), msg.size()) < 0) {
            err_fun();
        }
    });
}

bool TcpConn::set_timeout(int rms, int wms)
{
    assert(buffer_);
    timeval* rt = nullptr, *wt = nullptr;
    timeval t1 = { rms / 1000, rms % 1000 * 1000 };
    timeval t2 = { wms / 1000, wms % 1000 * 1000 };

    if (rms > 0) {
        rt = &t1;
    }
    if (wms > 0) {
        wt = &t2;
    }
    return bufferevent_set_timeouts(buffer_, rt, wt) != -1;
}

void TcpConn::read_handle(struct bufferevent *bev, void *ctx)
{
    auto self = (TcpConn*)ctx;
    auto input = bufferevent_get_input(bev);
    auto len = evbuffer_get_length(input);

    while (len > 0)
    {
        auto clen = evbuffer_get_contiguous_space(input);
        auto cbuf = evbuffer_pullup(input, clen);
        if (self->message_fun_) {
            self->message_fun_(self->shared_this(), cbuf, clen);
        }
        len -= clen;
        evbuffer_drain(input, clen);
    }
}

void TcpConn::event_handle(struct bufferevent *bev, short what, void *ctx)
{
    auto self = (TcpConn*)ctx;
    if (what & (BEV_EVENT_EOF | BEV_EVENT_ERROR)) 
    {
        std::error_code ec;
        if (what & BEV_EVENT_ERROR) {
            ec = make_ssl_error_code(bufferevent_get_openssl_error(bev));
        }
        else {
            ec = std::make_error_code(std::errc::connection_reset);
        }
        self->status_ = ConnState::Disconnected;
        if (self->disconnect_fun_) {
            self->disconnect_fun_(self->shared_this(), ec);
        }
        if (self->close_fun_) {
            self->close_fun_(self->shared_this());
        }
    }

    if(what & BEV_EVENT_CONNECTED)
    {
        self->status_.store(ConnState::Connected);
        if (self->connect_fun_) {
            self->connect_fun_(self->shared_this());
        }
    }
}

std::error_code TcpConn::update(ez_socket_t fd, int ssl_state, const sockaddr_storage& addr)
{
    fd_ = fd;

    remote_endpoint_ = addr;

    if (ssl_)
    {
        buffer_ = bufferevent_openssl_socket_new(loop_->event_base(), fd, ssl_,
            (bufferevent_ssl_state)ssl_state,
            BEV_OPT_CLOSE_ON_FREE | BEV_OPT_DEFER_CALLBACKS);
    }
    else {
        buffer_ = bufferevent_socket_new(loop_->event_base(), fd,
            BEV_OPT_CLOSE_ON_FREE | BEV_OPT_DEFER_CALLBACKS);
    }
    if (buffer_ == nullptr) {
        return make_custom_error_code(custom_error::bev_calloc_fail);
    }
    return std::error_code();
}

std::error_code TcpConn::attach(ez_socket_t fd, const sockaddr_storage& addr)
{
    auto ec = update(fd, BUFFEREVENT_SSL_ACCEPTING, addr);
    if (ec) {
        return ec;
    }
    return enable_event();
}

std::error_code TcpConn::dial(ez_socket_t fd, const sockaddr_storage& addr)
{
    assert(status_.load() == ConnState::Disconnected && buffer_ == nullptr);

    auto ec = update(fd, BUFFEREVENT_SSL_CONNECTING, addr);
    if (ec) {
        return ec;
    }

    if (bufferevent_socket_connect(buffer_, (struct sockaddr*)&addr, sizeof(addr)) < 0) {
        return make_socket_error_code(EVUTIL_SOCKET_ERROR());
    }

    type_ = ConnType::Outgoing;
    return enable_event();
}

std::error_code TcpConn::attach_proxy(ez_socket_t fd)
{
    assert(status_.load() == ConnState::Disconnected && buffer_ == nullptr);

    sockaddr_storage addr = {};
    socklen_t slen = sizeof(addr);
    getpeername(fd, (sockaddr*)&addr, &slen);

    auto ec = update(fd, BUFFEREVENT_SSL_CONNECTING, addr);
    if (ec) {
        return ec;
    }

    if (bufferevent_socket_connect(buffer_, nullptr, 0) < 0) {
        return make_socket_error_code(EVUTIL_SOCKET_ERROR());
    }

    type_ = ConnType::Outgoing;
    return enable_event();
}

std::error_code TcpConn::enable_event()
{
    // 在这儿获取本地的套接字信息
    init_local_by_fd(fd_);
    if (bufferevent_enable(buffer_, EV_READ | EV_WRITE) < 0) {
        return make_socket_error_code(EVUTIL_SOCKET_ERROR());
    }
    bufferevent_setcb(buffer_, read_handle, NULL, event_handle, (void*)this);

    status_ = ConnState::Connected;
    return std::error_code();
}

void TcpConn::close()
{
    if (status_ == ConnState::Disconnecting || status_ == ConnState::Disconnected) {
        return;
    }

    status_ = ConnState::Disconnecting;
    auto c = shared_from_this();
    auto f = [c]() {
        if (c->buffer_ == nullptr) {
            return;
        }
        bufferevent_free(c->buffer_);
        c->buffer_ = nullptr;
        c->status_.store(ConnState::Disconnected);
        if (c->close_fun_) {
            // 通知tcoserver 从容器中移除自己，好关闭事件池
            c->close_fun_(c);
        }
    };

    // 客户端可能会在回调函数中关闭连接，所以让关闭发生在一下次事件循环
    loop_->post(f);

}




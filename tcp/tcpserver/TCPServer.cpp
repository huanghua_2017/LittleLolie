
#include "TCPServer.h"
#include "../listener/Listener.h"
#include "../../loop/LoopService.h"
#include "../../loop/LoopThreadPool.h"
#include "../../common/ErrorCode.h"
#include "event2/util.h"
#include <assert.h>
#include <algorithm>


TCPServer::TCPServer(int thread_num)
{
    assert(thread_num > 0);
    pool_ = std::make_shared<LoopThreadPool>(thread_num);
    loop_ = pool_->next_loop();
}

TCPServer::~TCPServer()
{
    assert(status_ == ServerState::Null || status_ == ServerState::Stopped);
}

std::error_code TCPServer::listen(const std::string& addr, const TLSConfigPtr& tls)
{
    tls_config_ = tls;
    using namespace std::placeholders;
    assert(listener_ == nullptr);
    listener_ = std::make_shared<Listener>(*loop_);
    listener_->set_accept_handle(std::bind(&TCPServer::accept_handle, this, _1, _2, _3));
    return listener_->listen(addr);
}

std::error_code TCPServer::run()
{
    assert(listener_ != nullptr);
    status_ = ServerState::Running;
    pool_->start();
    return std::error_code();
}

void TCPServer::stop()
{
    assert(listener_ != nullptr);

    if (status_.load() == ServerState::Stopping || status_.load() == ServerState::Stopped) {
        return;
    }

    status_ = ServerState::Stopping;

    loop_->dispatch([this]() {
        listener_->close();
        for (auto& c : connections_){
            c->close();
        }
    });
    
}

void TCPServer::accept_handle(ez_socket_t fd, const struct sockaddr *addr, int socklen)
{
    TcpConnPtr conn;

    auto lp = pool_->next_loop();
    if (tls_config_) {
        auto ssl = tls_config_->create_ssl();
        if (ssl == nullptr) {
            evutil_closesocket(fd);
            return;
        }
        conn = std::make_shared<TcpConn>(lp, ssl);
    }
    else {
        conn = std::make_shared<TcpConn>(lp, nullptr);
    }
    
    conn->set_disconnect_handle(disconnect_fun_);
    conn->set_close_fun(std::bind(&TCPServer::tcp_conn_close_handle, this, std::placeholders::_1));
    conn->set_message_handle(message_fun_);

    sockaddr_storage ss = {};
    memcpy(&ss, addr, std::min<int>(socklen, sizeof(ss)));
    // 监听线程和conn的事件循环不是一个线程所有需要由conn的线程进行操作
    lp->dispatch([=]() {
        if (conn->attach(fd, ss)) {
            evutil_closesocket(fd);
            return;
        }
        connections_.insert(conn);
        if (connect_fun_) {
            connect_fun_(conn);
        }
    });
}

void TCPServer::tcp_conn_close_handle(const TcpConnPtr& conn)
{
    // 这个函数由conn线程调用过来的，所有需要切换到当前线程进行处理
    loop_->dispatch([this, conn]() {
        // Remove the connection in the listening EventLoop
        this->connections_.erase(conn);
        if (status_.load() == ServerState::Stopping && this->connections_.empty()) {
            pool_->stop();
            pool_.reset();
            status_ = ServerState::Stopped;
        }
    });
}

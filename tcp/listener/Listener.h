
#pragma once

#include <string>
#include <system_error>
#include <functional>
#include "../../common/platform.h"

struct event_base;
struct evconnlistener;




class LoopService;

typedef std::function<void(ez_socket_t, const struct sockaddr *, int)> accept_handle;


class Listener
{
public:
    Listener(LoopService& base);
    ~Listener();

    evconnlistener* listener() {
        return handler_;
    }

    LoopService* loop() {
        return loop_;
    }

    void close();

    std::error_code listen(const std::string& addr);

    void set_accept_handle(const accept_handle& cb) {
        callback_ = cb;
    }

private:
    static void listener_cb(struct evconnlistener *, ez_socket_t fd,
        struct sockaddr *addr, int socklen, void *arg);
private:
    LoopService * loop_;
    struct evconnlistener   *handler_;
    accept_handle           callback_;
};


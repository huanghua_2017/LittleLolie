
#pragma once

#include <atomic>

enum class ServerState {
    Null = 0,
    Running = 1,
    Stopping = 2,
    Stopped = 3,
};

class ServerStatus
{
public:
    ServerStatus() : status_(ServerState::Null){}
    virtual ~ServerStatus() {}

    ServerState status() const {
        return status_;
    }
protected:
    std::atomic<ServerState>             status_;
};

#include "ErrorCode.h"

#include <event2/util.h>
#include <openssl/err.h>

std::string socket_category::message(int value) const
{
    if (value == INT_MIN) {
        return "未知错误!";
    }
    return evutil_gai_strerror(value);
}

std::string openssl_category::message(int value) const
{
    if (value == INT_MIN) {
        return "openssl 未知错误!";
    }
    char szErrMsg[1024] = { 0 };
    ERR_error_string(value, szErrMsg); // 格式：error:errId:库:函数:原因
    return szErrMsg;
}

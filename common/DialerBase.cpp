
#include "DialerBase.h"
#include "../loop/LoopService.h"
#include <assert.h>


DialerBase::DialerBase()
{
    create_myself_ = true;
    loop_ = new LoopService;
}

DialerBase::DialerBase(LoopService* loop)
{
    assert(loop != nullptr);
    create_myself_ = false;
    loop_ = loop;
}

DialerBase::~DialerBase()
{
    if (create_myself_ && loop_) {
        loop_->stop();
        delete loop_;
    }
}

void DialerBase::run()
{
    assert(create_myself_);
    loop_->run();
}

void DialerBase::stop()
{
    assert(create_myself_);
    loop_->stop();
}

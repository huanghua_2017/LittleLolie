
#pragma once

class LoopService;

class DialerBase
{
public:
    DialerBase();
    explicit DialerBase(LoopService* loop);
    virtual ~DialerBase();

    LoopService* loop() {
        return loop_;
    }

    void run();

    void stop();

protected:
    LoopService             *loop_ = nullptr;
    bool                    create_myself_;
};
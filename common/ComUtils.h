
#pragma once

#include "../common/platform.h"
#include <vector>
#include <string>
#include <system_error>

struct sockaddr;
class EvURI;

std::string addr_to_ipport(const sockaddr* addr);

std::error_code dns_resolve(const std::string& host, int port, std::vector<sockaddr_storage>& addr);

std::error_code uri_dns_resolve(const EvURI& uri, sockaddr_storage& endpoint);
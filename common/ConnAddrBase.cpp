
#include "ConnAddrBase.h"
#include "ComUtils.h"
#include "ErrorCode.h"
#include <event2/util.h>

ConnAddrBase::ConnAddrBase()
{
    memset(&remote_endpoint_, 0, sizeof(remote_endpoint_));
    memset(&local_endpoint_, 0, sizeof(local_endpoint_));
}

const std::string& ConnAddrBase::remote_addr_string() const
{
    if (remote_addr_string_.empty()) {
        remote_addr_string_ = addr_to_ipport((sockaddr*)&remote_endpoint_);
    }
    return remote_addr_string_;
}

const std::string& ConnAddrBase::local_addr_string() const
{
    if (local_addr_string_.empty()) {
        local_addr_string_ = addr_to_ipport((sockaddr*)&local_endpoint_);
    }
    return local_addr_string_;
}

std::error_code ConnAddrBase::init_local_by_fd(ez_socket_t fd)
{
    socklen_t len = sizeof(local_endpoint_);
    if (::getsockname(fd, (sockaddr*)&local_endpoint_, &len) == -1) {  
        return make_socket_error_code(EVUTIL_SOCKET_ERROR());
    }
    return std::error_code();
}


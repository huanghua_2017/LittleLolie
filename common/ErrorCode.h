#pragma once

#include <system_error>

// 自定义错误
enum class custom_error
{
    no_error = 0,
    tls_init_error,
    invalid_uri,
    dns_resolve_error,
    bev_calloc_fail,
    pool_bad_status,
    evconn_calloc_fail,
    evrequest_calloc_fail,
    ws_httpstatus_error,
    // http 错误描述
    EV_HTTP_TIMEOUT,
    EV_HTTP_EOF,
    EV_HTTP_INVALID_HEADER,
    EV_HTTP_BUFFER_ERROR,
    EV_HTTP_REQUEST_CANCEL,
    EV_HTTP_DATA_TOO_LONG,
};
class custom_category : public std::error_category
{
public:
    char const * name() const noexcept {
        return "custom_category error";
    }

    std::string message(int value) const 
    {
        switch ((custom_error)value)
        {
        case custom_error::tls_init_error:
            return "TLS初始化失败";
        case custom_error::invalid_uri:
            return "无效的uri地址";
        case custom_error::dns_resolve_error:
            return "DNS解析失败";
        case custom_error::bev_calloc_fail:
            return "libevent.bufferevent分配失败";
        case custom_error::pool_bad_status:
            return "ThreadPool.Status错误";
        case custom_error::evconn_calloc_fail:
            return "libevent.evhttp_connection分配失败";
        case custom_error::evrequest_calloc_fail:
            return "libevent.evhttp_request分配失败";
        case custom_error::ws_httpstatus_error:
            return "websocket返回http状态码错误";
        case custom_error::EV_HTTP_TIMEOUT:
            return "libevent.http请求超时";
        case custom_error::EV_HTTP_EOF:
            return "libevent.http连接被关闭";
        case custom_error::EV_HTTP_INVALID_HEADER:
            return "libevent.http无效的请求头";
        case custom_error::EV_HTTP_BUFFER_ERROR:
            return "libevent.http.buffer分配失败";
        case custom_error::EV_HTTP_REQUEST_CANCEL:
            return "libevent.http请求被取消";
        case custom_error::EV_HTTP_DATA_TOO_LONG:
            return "libevent.http请求数据太长";
        default:
            return "";
        }                      
        return "";         
    }                          
};

inline const std::error_category& get_custom_category()
{
    static custom_category instance;
    return instance;
}

inline std::error_code make_custom_error_code(custom_error e)
{
    return std::error_code(static_cast<int>(e), get_custom_category());
}

template<>
struct std::is_error_code_enum<custom_error> : std::true_type{};

///////////////////////////////////////////////////////////////////////
// socket error libevent

class socket_category : public std::error_category
{
public:
    char const * name() const noexcept {
        return "socket_category error";
    }

    std::string message(int value) const;
};

inline const std::error_category& get_socket_category()
{
    static socket_category instance;
    return instance;
}

inline std::error_code make_socket_error_code(int e)
{
    if (e == 0) {
        e = INT_MIN;
    }
    return std::error_code(e, get_socket_category());
}


///////////////////////////////////////////////////////////////////////
// openssl error

class openssl_category : public std::error_category
{
public:
    char const * name() const noexcept {
        return "socket_category error";
    }

    std::string message(int value) const;
};

inline const std::error_category& get_openssl_category()
{
    static openssl_category instance;
    return instance;
}

inline std::error_code make_ssl_error_code(int e)
{
    if (e == 0) {
        e = INT_MIN;
    }
    return std::error_code(e, get_openssl_category());
}


#pragma once

#ifdef _WIN32
#include <WinSock2.h>
//struct sockaddr;
//struct sockaddr_storage;
#define ez_socket_t intptr_t
#else
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <netinet/tcp.h>
#define ez_socket_t int
#endif

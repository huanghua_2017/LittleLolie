
#pragma once

#include "../common/platform.h"
#include <string>
#include <system_error>


class ConnAddrBase
{
public:
    ConnAddrBase();
    virtual ~ConnAddrBase() = default;

    const sockaddr_storage& remote_endpoint() const {
        return remote_endpoint_;
    }
    const sockaddr_storage& local_endpoint() const {
        return local_endpoint_;
    }
    const std::string& remote_addr_string() const;
    const std::string& local_addr_string() const;
protected:
    std::error_code init_local_by_fd(ez_socket_t fd);
protected:
    sockaddr_storage    remote_endpoint_;
    sockaddr_storage    local_endpoint_;
    mutable std::string remote_addr_string_;
    mutable std::string local_addr_string_;
};
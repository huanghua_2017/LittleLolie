
#pragma once

#include <atomic>


enum class ConnType {
    Incoming = 0, // The type of a Conn held by a Server
    Outgoing = 1, // The type of a Conn held by a Client
};
enum class ConnState {
    Disconnected = 0,
    Connecting = 1,
    Connected = 2,
    Disconnecting = 3,
};

class ConnStatus
{
public:
    ConnStatus():type_(ConnType::Outgoing),status_(ConnState::Disconnected){}
    virtual ~ConnStatus() {}

    ConnType type() const {
        return type_;
    }
    ConnState status() const {
        return status_;
    }

protected:
    ConnType                    type_;
    std::atomic<ConnState>     status_;
};
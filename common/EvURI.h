
#pragma once

#include <string>
#include <system_error>



struct evhttp_uri;

class EvURI
{
public:
    EvURI() :uri_(nullptr) {}
    ~EvURI();

    static int get_default_port(const char* schem);

    std::error_code parse_from_string(const std::string& uriStr);

    std::string schem() const {
        return schem_;
    }
    std::string userinfo() const {
        return userinfo_;
    }
    std::string host() const {
        return host_;
    }
    int port() const {
        return port_;
    }
    std::string path() const {
        return path_;
    }
    std::string query() const {
        return query_;
    }
    std::string fragment() const {
        return fragment_;
    }
private:
    evhttp_uri * uri_ = nullptr;
    std::string schem_;
    std::string userinfo_;
    std::string host_;
    int port_;
    std::string path_;
    std::string query_;
    std::string fragment_;
};
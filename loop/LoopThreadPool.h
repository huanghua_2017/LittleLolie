#pragma once

#include <vector>
#include <thread>
#include <memory>
#include <atomic>
#include <system_error>


struct event_base;
class LoopService;

class LoopThreadPool
{
    LoopThreadPool(const LoopThreadPool&) = delete;
    void operator=(const LoopThreadPool&) = delete;
public:
	LoopThreadPool(int thread_num = 1);
	~LoopThreadPool();

	std::error_code start();

	void stop();

    // @brief Reinitialize some data fields after a fork
    void after_fork();

	LoopService* next_loop();

    LoopService* find_loop_by_base(event_base* b);
private:
	enum Status {
		kNull = 0,
		kRunning = 1,
		kStopping = 2,
		kStopped = 3,
	};
	std::atomic<Status>						status_;		// true runing
	int										thread_num_;
	std::atomic<int>						thread_run_num_;
	std::atomic<int>						thread_stop_num_;
	int										next_loop_id_;
	std::vector<LoopService*>					loops_;
	std::vector<std::thread>				threads_;
};



#pragma once

#include <string>
#include <memory>
#include <system_error>
#include <functional>

struct ssl_st;
struct ssl_ctx_st;
struct x509_store_ctx_st;


class TLSConfig;
typedef std::shared_ptr<TLSConfig> TLSConfigPtr;

class TLSConfig
{
    TLSConfig(const TLSConfig&) = delete;
    void operator=(const TLSConfig&) = delete;
public:
    enum class SSLFileType : int
    {
        PEM = 1,
        ASN1 = 2,
    };
    TLSConfig();
    virtual ~TLSConfig();

    static TLSConfigPtr make_ssl_config();
    
    // 生成ssl句柄
    ssl_st * create_ssl();

    // 私钥如果有密码的话
    void set_password_callback(const std::function<std::string()>& f);
    // 加载自己的私钥  
    std::error_code set_privateKey_file(const std::string& privateKey_file, SSLFileType type);
    // 加载自己的证书  
    std::error_code set_certificate_file(const std::string& certificate_file, SSLFileType type);
    //是否对服务器证书进行验证， true 验证 default false
    void set_verify(bool verify);
    // 加载CA的证书 
    std::error_code set_verify_locations(const std::string& verify_locations);
    //
    std::error_code set_certificate_chain_file(const std::string& cert_chain);
    // 当使用RSA算法鉴别的时候，会有一个临时的DH密钥磋商发生。这样会话数据将用这个临时的密钥加密，而证书中的密钥中做为签名。
    // 所以这样增强了安全性，临时密钥是在会话结束消失的，所以就是获取了全部信息也无法把通信内容给解密出来
    std::error_code set_tmp_dh_file(const std::string& tmp);

private:
    int ssl_file_type(SSLFileType type);
    // 判定私钥是否正确 
    std::error_code check_cer_pri();
    // 初始化
    bool init_ctx();
private:
    std::string     privateKey_file_;
    std::string     certificate_file_;
    std::string     verify_locations_;
    bool		    verify_;				// 是否验证对方的证书
    //////////////////////////////////////////////////////////////////////////
    ssl_ctx_st*        ctx_;
};
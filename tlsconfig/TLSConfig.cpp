
#include "TLSConfig.h"
#include "../common/ErrorCode.h"
#include <openssl/bio.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/rand.h>
#include <openssl/x509v3.h>
#include <assert.h>
#include <string>




struct ssl_init_tmp__ {
    ssl_init_tmp__() {
#if OPENSSL_VERSION_NUMBER >= 0x10100003L
        if (OPENSSL_init_ssl(OPENSSL_INIT_LOAD_CONFIG, NULL) == 0) {
            fprintf(stderr, "OPENSSL_init_ssl() failed\n");
            return;
        }
        /*
        * OPENSSL_init_ssl() may leave errors in the error queue
        * while returning success
        */
        ERR_clear_error();
#else
        SSLeay_add_ssl_algorithms();
        OpenSSL_add_all_algorithms();
        SSL_load_error_strings();
        ERR_load_BIO_strings();
#endif
    }
    ~ssl_init_tmp__() {
     
    }
} __tmp;

static int cert_verify_callback1(int ok, X509_STORE_CTX *ctx)
{
    return ok;
}

TLSConfig::TLSConfig()
    : verify_(false)
    , ctx_(nullptr)
{
}

TLSConfig::~TLSConfig()
{
    if (ctx_) {
        SSL_CTX_free(ctx_);
    }
}

TLSConfigPtr TLSConfig::make_ssl_config()
{
    TLSConfigPtr c = std::make_shared<TLSConfig>();
    if (!c->init_ctx()) {
        return nullptr;
    }
    return c;
}

SSL* TLSConfig::create_ssl()
{
    assert(ctx_);
    return SSL_new(ctx_);
}

bool TLSConfig::init_ctx()
{
    assert(ctx_ == nullptr);
    // ����ʹ��SSL V3,V2      
    ctx_ = SSL_CTX_new(SSLv23_method());
    return ctx_ != nullptr;
}

void TLSConfig::set_password_callback(const std::function<std::string()>& f)
{
    assert(ctx_ && f);
    SSL_CTX_set_default_passwd_cb_userdata(ctx_, (void*)f().c_str());
}

std::error_code TLSConfig::set_privateKey_file(const std::string& privateKey_file, SSLFileType type)
{
    assert(ctx_ && !privateKey_file.empty());
    privateKey_file_ = privateKey_file;
    if (SSL_CTX_use_PrivateKey_file(ctx_, privateKey_file_.c_str(), ssl_file_type(type)) <= 0) {
        return make_ssl_error_code(ERR_get_error());
    }
    return check_cer_pri();
}

std::error_code TLSConfig::set_certificate_file(const std::string& certificate_file, SSLFileType type)
{
    assert(ctx_ && !certificate_file.empty());
    certificate_file_ = certificate_file;
    if (SSL_CTX_use_certificate_file(ctx_, certificate_file_.c_str(), ssl_file_type(type)) <= 0) {
        return make_ssl_error_code(ERR_get_error());
    }
    return check_cer_pri();
}

std::error_code TLSConfig::set_verify_locations(const std::string& verify_locations)
{
    assert(ctx_ && !verify_locations.empty());
    verify_locations_ = verify_locations;
    if (SSL_CTX_load_verify_locations(ctx_, verify_locations_.c_str(), NULL) <= 0) {
        return make_ssl_error_code(ERR_get_error());
    }
    return std::error_code();
}

std::error_code TLSConfig::set_certificate_chain_file(const std::string& cert_chain)
{
    assert(ctx_ && !cert_chain.empty());
    if (SSL_CTX_use_certificate_chain_file(ctx_, cert_chain.c_str()) <= 0) {
        return make_ssl_error_code(ERR_get_error());
    }
    return std::error_code();
}

std::error_code TLSConfig::set_tmp_dh_file(const std::string& tmp)
{
    assert(ctx_ && !tmp.empty());
    auto bio = BIO_new_file(tmp.c_str(), "r");
    if (!bio) {
        return make_ssl_error_code(ERR_get_error());
    }
  
    auto dh = PEM_read_bio_DHparams(bio, 0, 0, 0);
    if (!dh) {
        BIO_free(bio);
        return make_ssl_error_code(ERR_get_error());
    }
        
    if (SSL_CTX_set_tmp_dh(ctx_, dh) == 1) {
        BIO_free(bio);
        return std::error_code();
    }
    DH_free(dh);
    return make_ssl_error_code(ERR_get_error());
}

void TLSConfig::set_verify(bool verify)
{
    assert(ctx_);
    verify_ = verify;
    SSL_CTX_set_verify(ctx_, verify_ ? SSL_VERIFY_PEER : SSL_VERIFY_NONE, cert_verify_callback1);
}

int TLSConfig::ssl_file_type(SSLFileType type)
{
    if (type == SSLFileType::ASN1) {
        return SSL_FILETYPE_ASN1;
    }
    return SSL_FILETYPE_PEM;
}

std::error_code TLSConfig::check_cer_pri()
{
    if (!privateKey_file_.empty() && !certificate_file_.empty() && !SSL_CTX_check_private_key(ctx_)) {
        return make_ssl_error_code(ERR_get_error());
    }
    return std::error_code();
}
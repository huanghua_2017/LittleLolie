
#pragma once

#include "../../tlsconfig/TLSConfig.h"
#include "../../common/ServerStatus.h"
#include "../../common/DialerBase.h"
#include <map>
#include <mutex>
#include <tuple>
#include <memory>
#include <vector>
#include <system_error>
#include <initializer_list>
#include <functional>
#include "../Request.h"
#include "ResponseWriter.h"




struct evhttp;
struct bufferevent;
struct event_base;
struct evhttp_request;

typedef std::function<void(RequestPtr, ResponseWriterPtr)>   router_fun;

// libevent http 框架没找到支持成多base的方法暂时只能使用单线程的方式实现
class HTTPServer : public DialerBase, public ServerStatus
{
public:

    HTTPServer();
    explicit HTTPServer(LoopService* loop);

    ~HTTPServer();

    void set_max_header_size(size_t size);

    void set_max_body_size(size_t size);

    // RequestPtr 和 ResponseWriterPtr 不要拷贝到其他线程中去使用
    // Allow为允许的httpmethod如果不指定则默认为所有请求方法都接收
    template<HTTPMethod... Allow, typename... AOP>
    bool register_handle(const std::string& path, const router_fun& f, AOP&&... aop);

    template<typename... AP, typename Function>
    void wrap(std::tuple<AP...> tp, std::vector<HTTPMethod> als,
        const Function& f, RequestPtr r, ResponseWriterPtr w);

    // 没找到路由的或者路由处理函数为空的都将调用这个注册的函数
    void register_default_handle(const router_fun& f);

    // ip:port
    std::error_code listen(const std::string& addr, const TLSConfigPtr& tls);

    std::error_code listen(const std::vector<std::string>& addr, const TLSConfigPtr& tls);

private:
    static struct bufferevent* create_evb(struct event_base *base, void *ctx);
    static void gen_http_router(struct evhttp_request *req, void *arg);
    static void default_router_handle(RequestPtr r, ResponseWriterPtr w);
    std::error_code _listen(const std::string& addr);
    void _start();
private:
    evhttp                          *httpd_;
    std::mutex                      r_mutex_;
    std::map<std::string, router_fun> router_;
    router_fun                      default_fun_;
    TLSConfigPtr                    tls_config_;
};

template<typename T, typename... ARGS>
struct has_filter
{
private:
    template<typename U> static auto Check(int) -> decltype(std::declval<U>().filter_handler(std::declval<ARGS>()...), std::true_type());
    template<typename U> static std::false_type Check(...);
public:
    enum { value = std::is_same<decltype(Check<T>(0)), std::true_type>::value };
};

template <typename... Args, typename F, std::size_t... Idx>
inline void for_each_tuple_l(std::tuple<Args...>& t, F&& f, std::index_sequence<Idx...>)
{
    std::initializer_list<int>{(std::forward<F>(f)(std::get<Idx>(t)), 0)...};
}

template<bool b, typename ITEM>
inline typename std::enable_if<b == true, bool>::type filter_handler(ITEM& item, RequestPtr r, ResponseWriterPtr w) {
    return item.filter_handler(r, w);
}
template<bool b, typename ITEM>
inline typename std::enable_if<b == false, bool>::type filter_handler(ITEM& item, RequestPtr r, ResponseWriterPtr w) {
    return true;
}

template<HTTPMethod... Allow, typename... AOP>
bool HTTPServer::register_handle(const std::string& path, const router_fun& f, AOP&&... aop)
{
    std::lock_guard<std::mutex> _(r_mutex_);

    auto it = router_.find(path);
    if (it != router_.end()) {
        return false;
    }

    router_[path] = [this, f, &aop...](RequestPtr r, ResponseWriterPtr w) {
        wrap(std::make_tuple(aop...), { Allow... },[=]() {
            // 调用用户自己注册的处理函数
            f(r, w);
        }, r, w);
    };
    return true;
}

template<typename... AP, typename Function>
void HTTPServer::wrap(std::tuple<AP...> tp, std::vector<HTTPMethod> als,
    const Function& f, RequestPtr r, ResponseWriterPtr w)
{
    bool ba = false;
    for (auto& allow : als) {
        if (allow == r->EMMothd) {
            ba = true;
            break;
        }
    }
    if (!ba && !als.empty()) {
        w->write(HTTPStatus::StatusMethodNotAllowed, "");
        return;
    }

    // 调用aop的过滤器，如果返回错误的话将不会调用业务函数了
    ba = true;
    for_each_tuple_l(tp, [this, &ba, &r, &w](auto& item) {
        if (!ba) return;
        ba = filter_handler<has_filter<decltype(item), RequestPtr, ResponseWriterPtr>::value>(item, r, w);
    }, std::make_index_sequence<sizeof...(AP)>{});
    if (!ba) {
        return;
    }
    

    // 调用wrap传入的函数
    f();
}

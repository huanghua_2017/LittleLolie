
#pragma once

#include "../HttpStatus.h"
#include <istream>
#include <memory>
#include <system_error>


struct evhttp_request;

class LoopService;
class Request;
class ResponseWriter;
typedef std::shared_ptr<ResponseWriter> ResponseWriterPtr;

class ResponseWriter : public std::enable_shared_from_this<ResponseWriter>
{
public:
    ResponseWriter(evhttp_request* r, LoopService* lp);

    // 调用 write_header 后不能在调用这个函数
    void set_header(const std::string& key, const std::string& val) {
        header_[key] = val;
    }
    void del_header(const std::string& key) {
        header_.erase(key);
    }

    // evhttp_request的生命周期是由libevent控制的
    // 请在当前回调的线程中完成数据的发送
    void write(HTTPStatus status, const void* data, int size);
    void write(HTTPStatus status, const std::string& msg);
    void write(HTTPStatus status, const std::istream& ios);

    void write_chunk_begin(HTTPStatus status);
    void write_chunk(const void* data, int size);
    void write_chunk(const std::string& msg);
    void write_chunk(const std::istream& ios);
    void write_chunk_end();

private:
    friend class Upgrader;
    bool                                chunk_; 
    LoopService                         *loop_;
    evhttp_request                      *rsp_;
    std::map<std::string, std::string>  header_;
};

#include "HTTPServer.h"
#include "../../loop/LoopService.h"
#include "../../loop/LoopThreadPool.h"
#include "../../common/ErrorCode.h"
#include <event2/util.h>
#include <event2/http.h>
#include <event2/bufferevent_ssl.h>
#include <event2/bufferevent.h>
#include <assert.h>


HTTPServer::HTTPServer() : DialerBase()
{
    httpd_ = evhttp_new(loop_->event_base());
}

HTTPServer::HTTPServer(LoopService* loop) : DialerBase(loop)
{
    httpd_ = evhttp_new(loop_->event_base());
}

HTTPServer::~HTTPServer()
{
    assert(status_ == ServerState::Null || status_ == ServerState::Stopped);
    evhttp_free(httpd_);
}

void HTTPServer::set_max_header_size(size_t size)
{
    assert(httpd_);
    evhttp_set_max_headers_size(httpd_, size);
}

void HTTPServer::set_max_body_size(size_t size)
{
    assert(httpd_);
    evhttp_set_max_body_size(httpd_, size);
}

void HTTPServer::register_default_handle(const router_fun& f)
{
    assert(f != nullptr);
    std::lock_guard<std::mutex> _(r_mutex_);
    default_fun_ = f;
}

std::error_code HTTPServer::_listen(const std::string& addr)
{
    assert(httpd_);
    
    auto pos = addr.find(":");
    if (pos == std::string::npos) {
        return std::make_error_code(std::errc::invalid_argument);
    }
    auto ip = addr.substr(0, pos);
    auto port = addr.substr(pos + 1, addr.size());
    if (ip.empty()) {
        ip = "0.0.0.0";
    }
    if (port.empty()) {
        return std::make_error_code(std::errc::invalid_argument);
    }
    if (evhttp_bind_socket(httpd_, ip.c_str(), atoi(port.c_str())) < 0) {
        return make_socket_error_code(EVUTIL_SOCKET_ERROR());
    }
    return std::error_code();
}

std::error_code HTTPServer::listen(const std::vector<std::string>& addr, const TLSConfigPtr& tls)
{
    tls_config_ = tls;
    for (auto& ip : addr) {
        auto ec = _listen(ip);
        if (ec) {
            return ec;
        }
    }
    return std::error_code();
}

std::error_code HTTPServer::listen(const std::string& addr, const TLSConfigPtr& tls)
{
    tls_config_ = tls;
    std::error_code ec = _listen(addr);
    if (ec) {
        return ec;
    }
    _start();
    return std::error_code();
}

void HTTPServer::_start()
{
    assert(status_ == ServerState::Null && httpd_ != nullptr);

    // create_evb 根据是否使用ssl创建buffer， 如果自己没有创建成功， libevent 将创建一个
    // 普通的socket bufferevent，有可能不是自己想要的结果
    evhttp_set_bevcb(httpd_, create_evb, (void*)this);
    evhttp_set_gencb(httpd_, gen_http_router, (void*)this);
    evhttp_set_default_content_type(httpd_, "text/plain;charset=utf-8");
    if (default_fun_ == nullptr) {
        using namespace std::placeholders;
        default_fun_ = std::bind(default_router_handle, _1, _2);
    }
    status_ = ServerState::Running;
}


struct bufferevent* HTTPServer::create_evb(struct event_base * b, void *ctx)
{
    printf("%s\n", __FUNCTION__);
    auto self = (HTTPServer*)ctx;
    if (self->tls_config_) { 
        auto ssl = self->tls_config_->create_ssl();
        if (ssl == nullptr) {
            fprintf(stderr, "create ssl context error!\n");
            return nullptr;
        }
        return bufferevent_openssl_socket_new(b, -1, ssl,
            BUFFEREVENT_SSL_ACCEPTING, BEV_OPT_CLOSE_ON_FREE | BEV_OPT_DEFER_CALLBACKS);
    }
    return bufferevent_socket_new(b, -1,
        BEV_OPT_CLOSE_ON_FREE | BEV_OPT_DEFER_CALLBACKS);
}

void HTTPServer::gen_http_router(struct evhttp_request *req, void *arg)
{
    printf("%s\n", __FUNCTION__);
    auto self = (HTTPServer*)arg;
    auto c = evhttp_request_get_connection(req);
    
    ResponseWriterPtr w = std::make_shared<ResponseWriter>(req, self->loop_);
    RequestPtr r = Request::create_incoming_request(req);
    if (r == nullptr) {
        w->write(HTTPStatus::StatusBadRequest, "");
        return;
    }
    
    auto& path = r->Path;
    router_fun fun;
    {
        std::lock_guard<std::mutex> _(self->r_mutex_);
        auto it = self->router_.find(path);
        if (it == self->router_.end() || it->second == nullptr) {
            fun = self->default_fun_;
        }
        else {
            fun = it->second;
        }
    }
    
    // 调用用户自定义的处理函数
    fun(r, w);
}

void HTTPServer::default_router_handle(RequestPtr r, ResponseWriterPtr w)
{
    w->write(HTTPStatus::StatusNotFound, "");
}

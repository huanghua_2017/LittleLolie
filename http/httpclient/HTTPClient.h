
#pragma once

#include "../../tlsconfig/TLSConfig.h"
#include "../../common/DialerBase.h"
#include "../../common/platform.h"
#include "../Request.h"
#include <map>
#include <vector>
#include <system_error>
#include <functional>


struct bufferevent;
struct evhttp_connection;
struct evhttp_request;

typedef std::function<void(const RequestPtr&, const std::error_code&)> complete_fun;

struct conn_info {
    bufferevent             *buffer_ = nullptr;
    evhttp_connection       *evconn_ = nullptr;
    evhttp_request          *evrequest_ = nullptr;
    complete_fun            complete_fun_;
    TLSConfigPtr            tls_config_;
    std::string             key_;
    void                    *self_; // this
	int						status_; // 0 - 可用, 1 - 忙碌
};

class HTTPClient : public DialerBase
{
public:
    
    using DialerBase::DialerBase;
    ~HTTPClient();

    void set_time_out(int ms) {
        timeout_ = ms;
    }
    
    void Do(const RequestPtr& req, const complete_fun& f, const TLSConfigPtr& tls);
private:
    static void http_request_handle(evhttp_request* r, void* arg);

    // 
    void Do_(const RequestPtr& req, const complete_fun& f, const TLSConfigPtr& tls);

    // 从当前所有连接中，选出到目标服务器的连接，如果不存在则新建一个
    std::error_code find_conn(const std::string& host, int port, 
        const TLSConfigPtr& tls, const complete_fun& f, conn_info*& conn);
protected:
    void clear_conns();
protected:
    // key 为 ip:port
    std::map<std::string, std::vector<conn_info*>>    conns_;
    int                 timeout_ = 0;
};
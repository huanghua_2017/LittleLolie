
#include "HTTPClient.h"
#include "../../loop/LoopService.h"
#include "../../common/ErrorCode.h"
#include "../../common/EvURI.h"
#include "../../proxy/ProxyBase.h"
#include <event2/http.h>
#include <event2/bufferevent.h>
#include <event2/bufferevent_ssl.h>
#include <sstream>

HTTPClient::~HTTPClient()
{
    clear_conns();
}

void HTTPClient::Do(const RequestPtr& req, const complete_fun& f, const TLSConfigPtr& tls)
{
	this->loop()->dispatch([=]() {Do_(req, f, tls); });
}

void HTTPClient::Do_(const RequestPtr& req, const complete_fun& f, const TLSConfigPtr& tls)
{
    if (req->RemoteHost.empty() || req->RemotePort == 0) {
		if (f != nullptr) {
			f(nullptr, std::make_error_code(std::errc::invalid_argument));
		}
		return;
    }
 
    conn_info* conn = nullptr;
    auto ec = find_conn(req->RemoteHost, req->RemotePort,tls, f, conn);
    if (ec) {
		if (f != nullptr) {
			f(nullptr, ec);
		}
		return;
    }

    auto output_headers = evhttp_request_get_output_headers(conn->evrequest_);
    for (auto& h : req->Header) {
        evhttp_add_header(output_headers, h.first.c_str(), h.second.c_str());
    }
    if (req->Header["Host"].empty()) {
        evhttp_add_header(output_headers, "Host", (req->RemoteHost + ":" + std::to_string(req->RemotePort)).c_str());
    }
	if (!req->Body.empty()) {
		auto bv = evhttp_connection_get_bufferevent(conn->evconn_);
		bufferevent_write(bv, req->Body.data(), req->Body.size());
	}
    evhttp_cmd_type ht = EVHTTP_REQ_GET;

    if (req->Method == "POST")         ht = EVHTTP_REQ_POST;
    else if (req->Method == "HEAD")    ht = EVHTTP_REQ_HEAD;
    else if (req->Method == "PUT")     ht = EVHTTP_REQ_PUT;
    else if (req->Method == "DELETE")   ht = EVHTTP_REQ_DELETE;
    else if (req->Method == "OPTIONS")  ht = EVHTTP_REQ_OPTIONS;
    else if (req->Method == "TRACE")   ht = EVHTTP_REQ_TRACE;
    else if (req->Method == "CONNECT")  ht = EVHTTP_REQ_CONNECT;
    else if (req->Method == "PATCH")    ht = EVHTTP_REQ_PATCH;

    std::string newuri = req->Query.empty() ? req->Path : (req->Path + "?" + req->Query);
    // 发起握手请求
    auto r = evhttp_make_request(conn->evconn_, conn->evrequest_, ht, newuri.c_str());
    if (r != 0) {
        // 出错了， 不用释放！在析构函数中进行释放
		f(nullptr, make_socket_error_code(EVUTIL_SOCKET_ERROR()));
		return;
    }
	conn->status_ = 1;
}


std::error_code HTTPClient::find_conn(const std::string& host, int port,
    const TLSConfigPtr& tls, const complete_fun& f, conn_info*& conn)
{
    std::stringstream istr;
    istr << host << ":" << port;

    auto it = conns_.find(istr.str());
    if (it != conns_.end()) {
		for (auto c : it->second){
			if (c->status_ == 0) {
				conn = c;
				return std::error_code();
			}
		}
    }

    bufferevent *bev = nullptr;
    evhttp_connection* evcon = nullptr;
    evhttp_request* evreq = nullptr;

    if (tls) {
        auto ssl = tls->create_ssl();
        if (ssl == nullptr) {
            return make_custom_error_code(custom_error::tls_init_error);
        }
        bev = bufferevent_openssl_socket_new(loop_->event_base(), -1, ssl,
            BUFFEREVENT_SSL_CONNECTING,
            BEV_OPT_CLOSE_ON_FREE | BEV_OPT_DEFER_CALLBACKS);
    }
    else {
        bev = bufferevent_socket_new(loop_->event_base(), -1, BEV_OPT_CLOSE_ON_FREE);
    }
    if (bev == nullptr) {
        return make_custom_error_code(custom_error::bev_calloc_fail);
    }

	auto fd = bufferevent_getfd(bev);
#ifdef WIN32
	char chOpt = 1;
	setsockopt(fd, IPPROTO_TCP, TCP_NODELAY, &chOpt, sizeof(char));
#else
	int optval = 1;
	::setsockopt(fd, IPPROTO_TCP, TCP_NODELAY,&optval, sizeof(optval));
#endif 
    evcon = evhttp_connection_base_bufferevent_new(loop_->event_base(), NULL, bev, host.c_str(), port);

    if (evcon == nullptr) {
        bufferevent_free(bev);
        return make_custom_error_code(custom_error::evconn_calloc_fail);
    }
    if (timeout_ > 0) {
        evhttp_connection_set_timeout(evcon, timeout_);
    }
    
    conn_info* nc = new conn_info;
    nc->complete_fun_ = f;
    nc->tls_config_ = tls;
    nc->buffer_ = bev;
    nc->evconn_ = evcon;
    nc->key_ = istr.str();
    nc->self_ = this;
	nc->status_ = 0;

    evreq = evhttp_request_new(http_request_handle, nc);
    if (evreq == NULL) {
        delete nc;
        evhttp_connection_free(evcon);
        return make_custom_error_code(custom_error::evrequest_calloc_fail);
    }
    
    nc->evrequest_ = evreq;
	conns_[istr.str()].push_back(nc);

    conn = nc;
    return std::error_code();
}

void HTTPClient::clear_conns()
{
    for (auto& it : conns_) {
		for (auto c : it.second){
			if (c->evconn_) {
				evhttp_connection_free(c->evconn_);
			}
			delete c;
		}
    }
    conns_.clear();
}

void HTTPClient::http_request_handle(evhttp_request* r, void* arg)
{
    conn_info *conn = (conn_info*)arg;
	conn->status_ = 0;
    if (conn->complete_fun_ == nullptr) {
        return;
    }
    std::error_code ec;
    if (r == nullptr) {
        // 握手错误, 调用close_fun通知
        if (conn->tls_config_) {
            ec = make_ssl_error_code(bufferevent_get_openssl_error(conn->buffer_));
        }
        else {
            ec = make_socket_error_code(EVUTIL_SOCKET_ERROR());
        }
        conn->complete_fun_(nullptr, ec);
        return;
    }

    auto rsp = Request::create_incoming_request(r);
    if (rsp == nullptr) {
        ec = std::make_error_code(std::errc::bad_message);
    }
    conn->complete_fun_(rsp, ec);
}


#pragma once

#include <stdint.h>
#include <map>
#include <string>
#include <ostream>
#include <memory>

struct evhttp_request;
struct evhttp_uri;
class Request;
typedef std::shared_ptr<Request> RequestPtr;

enum class HTTPMethod
{
    REQ_NULL        = 0     ,
    REQ_GET         = 1 << 0,
    REQ_POST        = 1 << 1,
    REQ_HEAD        = 1 << 2,
    REQ_PUT         = 1 << 3,
    REQ_DELETE      = 1 << 4,
    REQ_OPTIONS     = 1 << 5,
    REQ_TRACE       = 1 << 6,
    REQ_CONNECT     = 1 << 7,
    REQ_PATCH       = 1 << 8,
};

// 发送出去的和收到的回复都是用这个类
class Request : public std::enable_shared_from_this<Request>
{
public:
    struct CaseCmper {
        bool operator()(const std::string& k1, const std::string& k2) const;
    };

    typedef std::map<std::string, std::string> Values;
    typedef std::map<std::string, std::string, CaseCmper> WeakValues;

    // only incoming
    static RequestPtr create_incoming_request(evhttp_request *r);
    // only incoming 解析http服务端返回信息
    static RequestPtr create_incoming_request(const std::string& str);
    // only outgoing
    static RequestPtr create_outgoing_request(const std::string& method, const std::string& uri, const std::string& body);

    ~Request() = default;
    
    // only incoming
    evhttp_request* request() {
        return req_;
    }

    // 只有服务端收到的请求才包含内容，服务器返回的不包含，发送出去的请求也不会解析uri到这里面
    Values          From;    // URL field's query parameters 

    // Method specifies the HTTP method (GET, POST, PUT, etc.).
    std::string     Method;
    // 
    HTTPMethod      EMMothd = HTTPMethod::REQ_NULL;

    std::string     Proto;// "HTTP/1.1"
    int             ProtoMajor = 1;    // 1
    int             ProtoMinor = 1;    // 1
    WeakValues      Header;
    std::string     RemoteHost;           //
    int             RemotePort = 0;           // 
    std::string     Uri;    // GET raw string
    std::string     Path; // (parsed)
    std::string     Query;

    // 发送出去的body或者收到的body
    std::string     Body;

    // 收到的回复
    int             Status = 0;
    std::string     StatusMsg;
private:
    Request() = default;
    // only incoming
    bool parse(evhttp_request *r);
    // 
    bool parse_frist_line(const char*& begin);
    bool parse_header_line(const char*& begin);
private:
    // only incoming
    evhttp_request *req_ = nullptr;   // 
};
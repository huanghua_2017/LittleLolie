
#include "Request.h"
#include "../common/ComUtils.h"
#include "../common/EvURI.h"
#include <event2/http.h>
#include <event2/http_struct.h>
#include <event2/keyvalq_struct.h>
#include <event2/buffer.h>
#include <event2/util.h>
#include <assert.h>
#include <sstream>


template<typename MAP>
inline void parse_keyval(evkeyvalq* ori, MAP& kv)
{
    if (ori == nullptr) {
        return;
    }
    for (auto it = ori->tqh_first; it != nullptr; it = it->next.tqe_next) {
        kv[it->key] = it->value;
    }
}


bool Request::CaseCmper::operator()(const std::string& k1, const std::string& k2) const
{
    if (evutil_ascii_strcasecmp(k1.c_str(), k2.c_str()) < 0) {
        return true;
    }
    return false;
}

RequestPtr Request::create_incoming_request(evhttp_request *r)
{
    assert(r != nullptr);
    RequestPtr hr = RequestPtr(new Request);
    if (!hr->parse(r)) {
        return nullptr;
    }
    return hr;
}


RequestPtr Request::create_incoming_request(const std::string& str)
{
    RequestPtr hr = RequestPtr(new Request);
    const char* begin = str.c_str();

    // ������һ��
    if (!hr->parse_frist_line(begin)) {
        return nullptr;
    }

    while (*begin) {
        if (!hr->parse_header_line(begin)) {
            return nullptr;
        }
    }
    return hr;
}

RequestPtr Request::create_outgoing_request(const std::string& method, const std::string& uriStr, const std::string& body)
{
    RequestPtr hr = RequestPtr(new Request);
    EvURI uri;
    if (!uri.parse_from_string(uriStr)) {
        hr->RemoteHost = uri.host();
        hr->RemotePort = uri.port();
        hr->Uri = uriStr;
        hr->Path = uri.path();
        hr->Query = uri.query();
    }

    hr->Method = method;
    hr->Proto = "HTTP/1.1";
    hr->ProtoMajor = 1;
    hr->ProtoMinor = 1;
    hr->Body = body;
    
    return hr;
}

bool Request::parse(evhttp_request *r)
{
    req_ = r;
    evhttp_cmd_type method = r->type;
    switch (method)
    {
    case EVHTTP_REQ_GET:        Method = "GET";     EMMothd = HTTPMethod::REQ_GET;   break;
    case EVHTTP_REQ_POST:       Method = "POST";    EMMothd = HTTPMethod::REQ_POST;  break;
    case EVHTTP_REQ_HEAD:       Method = "HEAD";    EMMothd = HTTPMethod::REQ_HEAD;  break;
    case EVHTTP_REQ_PUT:        Method = "PUT";     EMMothd = HTTPMethod::REQ_PUT; break;
    case EVHTTP_REQ_DELETE:     Method = "DELETE";  EMMothd = HTTPMethod::REQ_DELETE; break;
    case EVHTTP_REQ_OPTIONS:    Method = "OPTIONS"; EMMothd = HTTPMethod::REQ_OPTIONS; break;
    case EVHTTP_REQ_TRACE:      Method = "TRACE";   EMMothd = HTTPMethod::REQ_TRACE; break;
    case EVHTTP_REQ_CONNECT:    Method = "CONNECT"; EMMothd = HTTPMethod::REQ_CONNECT; break;
    case EVHTTP_REQ_PATCH:      Method = "PATCH";   EMMothd = HTTPMethod::REQ_PATCH; break;
    default: break;
    }
    Proto = "HTTP/" + std::to_string(r->major) + "." + std::to_string(r->minor);
    ProtoMajor = r->major;
    ProtoMinor = r->minor;
    parse_keyval(r->input_headers, Header);
    RemoteHost = r->remote_host ? r->remote_host : "";
    RemotePort = r->remote_port;
    
    Uri = r->uri ? r->uri : "";
    EvURI uri;
    if (!uri.parse_from_string(Uri)) {
        Path = uri.path();
        evkeyvalq qkv = {};
        evhttp_parse_query_str(uri.query().c_str(), &qkv);
        parse_keyval(&qkv, From);
        evhttp_clear_headers(&qkv);
    }

    evbuffer *input = evhttp_request_get_input_buffer(req_);
    auto len = evbuffer_get_length(input);
    Body.append((const char*)evbuffer_pullup(input, len), len);
    evbuffer_drain(input, len);

    Status = evhttp_request_get_response_code(r);
    const char* cl = evhttp_request_get_response_code_line(r);
    StatusMsg = cl ? cl : "";
    return true;
}

bool Request::parse_frist_line(const char*& begin)
{
    const char* end = strstr(begin, "\r\n");
    if (end == nullptr) {
        return false;
    }
    if (strncmp(begin, "HTTP/", 5) != 0) {
        return false;
    }
    ProtoMajor = atoi(begin + 5);
    ProtoMinor = atoi(begin + 7);
    Proto.append(begin, 8);
    begin += 9;
    Status = atoi(begin);
    const char* sp = strstr(begin, " ");
    if (sp == nullptr) {
        return false;
    }
    StatusMsg.append(sp + 1, end);
    begin = end + 2;
    return true;
}

bool Request::parse_header_line(const char*& begin)
{
    if (strncmp(begin, "\r\n", 2) == 0) {
        begin += 2;
        return true;
    }
    const char* colon = strstr(begin, ":");
    const char* end = strstr(begin, "\r\n");
    if (colon == nullptr || end == nullptr) {
        return false;
    }
    std::string key, val;
    while (begin != colon) {
        if (!isspace(*begin)) {
            key.push_back(*begin);
        }
        ++begin;
    }
    colon++;
    while (colon != end) {
        if (!isspace(*colon)) {
            val.push_back(*colon);
        }
        ++colon;
    }
    Header[key] = val;
    begin = end + 2;
    return true;
}


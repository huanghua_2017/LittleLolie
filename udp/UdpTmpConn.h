

#pragma once

#include "../common/ConnAddrBase.h"
#include <memory>



class LoopService;

class UdpTmpConn;
typedef std::shared_ptr<UdpTmpConn>     UdpTmpConnPtr;


// 只是临时保存一些数据，提供发送接口
class UdpTmpConn : public ConnAddrBase, public std::enable_shared_from_this<UdpTmpConn>
{
public:
    UdpTmpConn(const sockaddr_storage& remote, const sockaddr_storage& local,
        LoopService* lp, ez_socket_t fd, bool b);
       
    ~UdpTmpConn();

    LoopService* loop() {
        return loop_;
    }

    bool out_going() const {
        return outgoing_;
    }

    std::error_code write(const std::string& msg) {
        return write(msg.data(), msg.size());
    }

    std::error_code write(const void* data, size_t len);
private:
    LoopService         *loop_ = nullptr;
    ez_socket_t         tmp_fd_ = -1;
    bool                outgoing_ = false; // false - 别人发过来的消息, true 自己主动发出去的
};
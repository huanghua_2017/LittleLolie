
#include "UdpTmpConn.h"
#include "../common/ComUtils.h"
#include "../common/ErrorCode.h"
#include <event2/util.h>


UdpTmpConn::UdpTmpConn(const sockaddr_storage& remote, const sockaddr_storage& local,
    LoopService* lp, ez_socket_t fd, bool b)
    : loop_(lp), tmp_fd_(fd), outgoing_(b)
{
    remote_endpoint_ = remote;
    local_endpoint_ = local;
}

UdpTmpConn::~UdpTmpConn()
{
}


std::error_code UdpTmpConn::write(const void* data, size_t len)
{
    int ret = sendto(tmp_fd_, (const char*)data, len, 0, (sockaddr*)&remote_endpoint_, sizeof(remote_endpoint_));
    if (ret == -1) {
        return make_socket_error_code(EVUTIL_SOCKET_ERROR());
    }
    return std::error_code();
}


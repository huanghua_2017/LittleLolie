
#pragma once

#include "UdpTmpConn.h"
#include "../common/DialerBase.h"
#include <system_error>
#include <functional>


struct event;

typedef std::function<void(const UdpTmpConnPtr&, const char*, int)>   upd_message_fun;


class UdpEventBase : public DialerBase
{
public:
    using DialerBase::DialerBase;

    ~UdpEventBase();
  
    void set_message_handle(const upd_message_fun& f) {
        upd_message_fun_ = f;
    }

private:
    static void event_handle(ez_socket_t fd, short what, void* arg);
protected:
    std::error_code attach(ez_socket_t fd);
protected:
    upd_message_fun         upd_message_fun_;
    ez_socket_t             fd_ = -1;
    event*                  event_ = nullptr;
    sockaddr_storage        local_point_; // 服务端为本地监听地址， 客户端为本地绑定, 需要子类设置这个字段
};
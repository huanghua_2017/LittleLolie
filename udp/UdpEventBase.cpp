
#include "UdpEventBase.h"
#include "../../loop/LoopService.h"
#include "../../common/ErrorCode.h"
#include <event2/util.h>
#include <event.h>
#include <assert.h>


UdpEventBase::~UdpEventBase()
{
    if (fd_ > 0) {
        evutil_closesocket(fd_);
    }
    if (event_) {
        event_del(event_);
        event_free(event_);
    }
}


void UdpEventBase::event_handle(ez_socket_t fd, short what, void* arg)
{
    UdpEventBase* self = (UdpEventBase*)arg;
    assert(fd == self->fd_);

    // 发生可读事件，读取套接字，并调用message_fun_;
    sockaddr_storage remote = {};
    socklen_t slen = sizeof(remote);
    char buf[1472] = {};

    int ret = recvfrom(fd, buf, 1472, 0, (sockaddr*)&remote, &slen);
    if (ret == -1) {
        // 发生错误了
        return;
    }
    
    auto conn = std::make_shared<UdpTmpConn>(remote, self->local_point_, self->loop_, self->fd_, false);

    if (self->upd_message_fun_) {
        self->upd_message_fun_(conn, buf, ret);
    }
}

std::error_code UdpEventBase::attach(ez_socket_t fd)
{
    if (fd_ > 0) {
        evutil_closesocket(fd_);
    }
    fd_ = fd;
    if (event_) {
        event_del(event_);
        event_free(event_);
    }
    event_ = event_new(loop_->event_base(), fd, EV_READ | EV_PERSIST, UdpEventBase::event_handle, (void*)this);
    if (event_ == nullptr) {
        return make_socket_error_code(EVUTIL_SOCKET_ERROR());
    }
    event_add(event_, NULL);

    return std::error_code();
}

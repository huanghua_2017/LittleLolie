
#pragma once

#include "../UdpEventBase.h"
#include "../UdpTmpConn.h"
#include <string>

class UdpClient : public UdpEventBase
{
public:
    using UdpEventBase::UdpEventBase;

    // uri example: udp://localhost:1111
    std::error_code dial(const std::string& uriStr);

    UdpTmpConnPtr conn() {
        return conn_;
    }
private:
    UdpTmpConnPtr   conn_;
};
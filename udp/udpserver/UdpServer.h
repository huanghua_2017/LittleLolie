
#pragma once

#include "../UdpEventBase.h"
#include <string>


class UdpServer : public UdpEventBase
{
public:
    using UdpEventBase::UdpEventBase;

    std::error_code listen(const std::string& ip_port);

};